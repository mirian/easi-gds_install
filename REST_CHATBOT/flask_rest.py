"""
Code of the api REST of chatbot. Run on the port 8000. There is two url available,
/messages with parameters question (the question asked by the user) and the idUser (use to
identify the user, to stock the conversation in the history, in sqlite database).
/history with the parameter idUser (to get all the messages send and received by this user).
"""
import os
import sqlite3
import sys
from flask import Flask, jsonify, request, g
from googletrans import Translator
from flask_cors import CORS
import chatbot_en
import chatbot_fr
import load_datas

#TRANSLATOR will be used to identify the language of the user's message.
TRANSLATOR = Translator(service_urls=["translate.google.fr", "translate.google.com"])
DATABASE = 'DB_HISTORY.db'
SQL = 'DB_HISTORY.sql'
NEO4J = None

APP = Flask(__name__)
CORS(APP)

#Our default password of neo4j database.
PASS = "123"

def get_db_sqlite():
    """
    Get the sqlite database instance.

    Returns
    -------
    database
        The sqlite database.
    """
    exist = os.path.exists(DATABASE)
    database = getattr(g, '_database', None)
    if database is None:
        database = g._database = sqlite3.connect(DATABASE)
    return database, exist

def find_language(message):
    """
    This function find in which language the message is (french or english).

    Parameters
    ----------
    message : string
        Question of the user.

    Returns
    -------
    string
        fr if the message is in french, en if it is in english.
    """
    if TRANSLATOR.detect(message).lang == "fr":
        return "fr"
    return "en"

@APP.route("/reload")
def reload():
    """
    Function to reload the information for the chatbot with the active neo4j database.

    Returns
    -------
    string
        Json format of the answer : {"database": string}.
    """
    load_datas.main(PASS, TYPE, NEO4J)

    #Reload the information of the new active neo4j database of the english model.
    chatbot_en.reload_informations()

    #Reload the information of the new active neo4j database of the french model.
    chatbot_fr.reload_informations()

    return jsonify({"response": "OK"})

@APP.route("/changeDB")
def change_database_neo4j():
    """
    Function of the get on /changeDB. There is one required parameter in the URL:
    - db which correspond to the name of the neo4j database.

    Returns
    -------
    string
        Json format of the answer : {"database": string}.
    """
    global NEO4J

    args = request.args
    NEO4J = args.get("db")

    dico = {"database": NEO4J}

    load_datas.main(PASS, TYPE, NEO4J)

    #Reload the information of the new active neo4j database of the english model.
    chatbot_en.reload_informations()

    #Reload the information of the new active neo4j database of the french model.
    chatbot_fr.reload_informations()

    return jsonify(dico)

@APP.route("/messages")
def get_messages():
    """
    Function of the get on /messages. There is three required parameters in the URL:
    -question which correspond to the question asked by the user.
    -idUser use to identify the user, to stock the conversation in the history, in sqlite database.
    -database use to identify the active neo4j database, to stock the conversation in the history
      of the user with this database.
    We store the question and the answer in the sqlite database with the idUser.
    Note: type_message => 0 == question, 1 == response.

    Returns
    -------
    string
        Json format of the answer : {"response": string, "link": string}.
    """
    args = request.args

    conn, _ = get_db_sqlite()

    message = args.get("question")

    #Choose which model we want to use (it depends of the language of the user's question).
    #And we create the response of the request.
    if message.strip():
        if find_language(message) == "en":
            response, link = chatbot_en.ask(message)
            dico = {"response": response, "link": link}
        else:
            response, link = chatbot_fr.ask(message)
            dico = {"response": response, "link": link}
    else:
        response = "Error, question empty"
        link = ""
        dico = {"response" : response, "link" : link}

    database = args.get("database")

    query = conn.cursor()

    #We store the question of the user in the sqlite database.
    query.execute("INSERT INTO MESSAGE (id_user, type_message, content, link, date_message," + \
                  " database_message) VALUES (?, ?, ?, ?, DATE('NOW'), ?)",
                (args.get("idUser"), 0, message, link, database)
    )

    #We store the answer in the sqlite database.
    query.execute("INSERT INTO MESSAGE (id_user, type_message, content, link, date_message," + \
                  " database_message) VALUES (?, ?, ?, ?, DATE('NOW'), ?)",
                (args.get("idUser"), 1, response, link, database)
    )

    conn.commit()
    conn.close()

    return jsonify(dico)

@APP.route("/history")
def get_history():
    """
    Function of the get on /history. There is two required parameters in URL:
    -idUser use to identify the user, to get the conversation in the history, in sqlite database.
    -database use to identify the active neo4j database, to get the conversation in the history
      of the user with this database.

    Returns
    -------
    string
        Json format of the answer : {"response": list({"type": integer, "content": string,
        "link": string})}.
    """
    args = request.args
    conn, _ = get_db_sqlite()

    #Get the message store in the sqlite database of the user with the id = idUser.
    history = conn.execute('SELECT * FROM MESSAGE WHERE id_user = ? and database_message = ?',
                           [args.get("idUser"), args.get("database")]).fetchall()

    res = {"response":[]}

    #Create the response for the request.
    if len(history) == 0:
        query = conn.cursor()
        query.execute("INSERT INTO MESSAGE (id_user, type_message, content, link, " + \
                  "date_message, database_message) VALUES (?, ?, ?, ?, DATE('NOW'), ?)",
                (args.get("idUser"), 1, "EASI-GDS Chatbot", "", args.get("database"))
        )
        res["response"].append({"type": 1, "content": "EASI-GDS Chatbot", "link": ""})
    else:
        for message in history:
            res["response"].append({"type": message[2], "content": message[3], "link": message[4]})

    conn.commit()
    conn.close()

    return jsonify(res)

if __name__ == '__main__':
    #Get the password of neo4j database in the bash arguments (if exists).
    if len(sys.argv) > 1:
        PASS = sys.argv[1]

    #Get the URL of neo4j database in the bash arguments (if exists).
    if len(sys.argv) > 2:
        TYPE = sys.argv[2]
    else:
        TYPE = "localhost"

    #Load all required data for the chatbot.
    load_datas.main(PASS, TYPE, NEO4J)

    #Launch the training of the english model.
    chatbot_en.main()

    #Launch the training of the french model.
    chatbot_fr.main()

    #Create sqlite databse.
    with APP.app_context():
        connection, exist = get_db_sqlite()

    if not exist:
        with open(SQL, encoding="utf-8") as f:
            connection.executescript(f.read())

    connection.commit()
    connection.close()

    #Run the app.
    APP.run(host='0.0.0.0', port=8000)
