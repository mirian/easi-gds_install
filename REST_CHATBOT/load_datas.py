"""
This file allows to get all required values for the chatbot.
We will identify, in all the node for all the properties, which one can be used to use the
classification, and/or regression, get all the label of nodes and all the label of relations.
"""
import json
from py2neo import Graph

EXPRESSION = """
CALL db.schema.nodeTypeProperties() yield nodeLabels, propertyName, propertyTypes
"""

LINK_EXPRESSION = "MATCH (n)-[r]->(m) RETURN DISTINCT type(r) AS relName, labels(n)" + \
    "AS startNode, labels(m) AS endNode"

def main(pwd, type, database_neo4j):
    """
    Main of the file. Launch the process to get the informations in the database.
    Parameters
    ----------
    pwd : string
        Password of the database.

    type : string
        Url of the database.

    database_neo4j : string
        Name of the nao4j database.
    """
    if database_neo4j is None:
        # connect to db
        graph = Graph("bolt://" + type + ":7687", auth=("neo4j", pwd))
    
    else:
        graph = Graph("bolt://" + type + ":7687", name = database_neo4j, auth=("neo4j", pwd))
         
       

    res = graph.run(EXPRESSION)

    dico = {}
    while res.forward():
        if str(res.current["nodeLabels"][0]) not in dico:
            dico[str(res.current["nodeLabels"][0])] = []
        dico[str(res.current["nodeLabels"][0])].append(
            (res.current["propertyName"], res.current["propertyTypes"]))

    predicted = {}
    predicted["regression"] = []
    predicted["classification"] = []
    predicted["linkPrediction"] = []
    predicted["label"] = []

    for label, props in dico.items():
        if label not in predicted["label"]:
            predicted["label"].append(label)
        for prop in props:

            if prop[1] == ['Long'] or prop[1] == ['String']:

                val = graph.run(
                    f"MATCH (n:{label}) WITH n.{prop[0]} AS var, n AS n RETURN " +
                    "count(distinct(var)),count(n),(count(distinct(var))*100/count(n))" +
                    " AS percentage")
                val.to_table()
                if val.current["percentage"] < 10:
                    predicted['classification'].append((label, prop[0]))
                else:
                    if prop[1] == ['Long']:
                        predicted['regression'].append((label, prop[0]))
            if prop[1] == ['Double']:
                predicted['regression'].append((label, prop[0]))

    res_link = graph.run(LINK_EXPRESSION)

    while res_link.forward():

        predicted["linkPrediction"].append(
            (res_link.current["relName"], [res_link.current["startNode"],
                                           res_link.current["endNode"]]))

    with open("information.json", "w", encoding="utf-8") as outfile:
        json.dump(predicted, outfile, indent=4)
