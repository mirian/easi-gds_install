"""
Code of the english chatbot. We will find the creation and the training of a model with the library
tensorflow keras. 
LINK from the tutorial
https://www.projectpro.io/article/python-chatbot-project-learn-to-build-a-chatbot-from-scratch/429
"""
import json
import string
import random
import nltk
import numpy as np
# import matplotlib.pyplot as plt
from nltk.stem import WordNetLemmatizer
import tensorflow as tf
from keras.layers import Dense
from keras.layers import Dropout
from keras import Sequential
#Used if we want to clean the text before the processing, here, we do not want.
# from nltk.corpus import stopwords

# nltk.download("stopwords")

# stop_words = set(stopwords.words('english'))

#Vocabulary of the model.
WORDS = []

#All the possible classes for a prediction.
CLASSES = []

#Name of the user.
NAME = []

#Tool used to use lemmatize function on text.
LEMMATIZER = WordNetLemmatizer()

#The future model for prediction.
MODEL = Sequential()

with open("./intents_en.json", encoding="utf-8") as file:
    data_file = file.read()

#Training datas.
DATA = json.loads(data_file)

with open("./information.json", encoding="utf-8") as file:
    infomation_database = file.read()

#Informations get from the neo4j database.
INFORMATION = json.loads(infomation_database)

def main():
    """
    This function launch the training process.
    """
    global WORDS
    global CLASSES
    global MODEL
    # global stop_words

    nltk.download('punkt')
    nltk.download('wordnet')

    data_x = []
    data_y = []

    for intent in DATA["intents"]:  # For each type of responses.
        for pattern in intent["patterns"]:  # For each questions in input.
            # We decompose the question in list of word ("Hello world" => ["Hello", "world"]).
            tokens = nltk.word_tokenize(pattern)
            WORDS.extend(tokens)
            data_x.append(pattern)
            data_y.append(intent["tag"])
        if intent["tag"] not in CLASSES:
            # Add the type of tag in the list of classes.
            CLASSES.append(intent["tag"])

    # words = [lemmatizer.lemmatize(word.lower(
    # )) for word in words if word not in string.punctuation and word not in stop_words]
    # Transform word in lower letter and delete the poncutation.
    WORDS = [LEMMATIZER.lemmatize(word.lower())
             for word in WORDS if word not in string.punctuation]
    WORDS = sorted(set(WORDS))  # Sort of the list of word (vocabulary).
    # Sort of the list of classes (types of answer).
    CLASSES = sorted(set(CLASSES))

    training = []

    out_empty = [0] * len(CLASSES)
    for idx, doc in enumerate(data_x):  # For each questions in input.
        bow = []
        text = LEMMATIZER.lemmatize(doc.lower())
        # We search at which position the word "doc" is in our vocabulary (words).
        for word in WORDS:
            if word in text:
                bow.append(1)
            else:
                bow.append(0)
        output_row = list(out_empty)
        # Get the index of the classe of the current question.
        output_row[CLASSES.index(data_y[idx])] = 1
        # We add our bow (one hot encoding for the word) and the corresponding class
        # (in one hot encoding too).
        training.append([bow, output_row])
    # training is a list of list of two elements : a word in one hot encoding and its
    # classes in one hot encoding ([[00001, 0010], [...]]).

    random.shuffle(training)
    training = np.array(training, dtype=object)
    # List of word in one hot encoding in training.
    train_x = np.array(list(training[:, 0]))
    # List of classes in one hot encoding in training.
    train_y = np.array(list(training[:, 1]))

    MODEL = Sequential()
    MODEL.add(Dense(128, input_shape=(len(train_x[0]),), activation="relu"))
    MODEL.add(Dropout(0, 5))
    MODEL.add(Dense(64, activation="relu"))
    MODEL.add(Dropout(0, 5))
    # Our model is compose of three layers, 128 neurals in inupt, then 64, to finish,
    # the number of classes in output.
    MODEL.add(Dense(len(train_y[0]), activation="softmax"))

    adam = tf.keras.optimizers.Adam(learning_rate=0.01)

    MODEL.compile(loss="categorical_crossentropy",
                  optimizer=adam, metrics="accuracy")
    # print(model.summary())
    MODEL.fit(x=train_x, y=train_y, epochs=150, verbose=1)  # Train a model.

    # history = MODEL.fit(x=train_x, y=train_y, epochs=150, verbose=1)

    # plt.plot(history.history['loss'])
    # plt.title('model loss')
    # plt.ylabel('loss')
    # plt.xlabel('epoch')
    # plt.legend(['train'], loc='upper left')
    # plt.show()

def clean_text(text):
    """
    This function clean the text from the user message.

    Parameters
    ----------
    text : string
        Text to clean.

    Returns
    -------
    list(string)
        List of the word in the text.
    """
    # global stop_words
    tokens = nltk.word_tokenize(text)
    # tokens = [lemmatizer.lemmatize(word.lower()) for word in tokens if word
    # not in string.punctuation and word not in stop_words]
    tokens = [LEMMATIZER.lemmatize(word.lower())
              for word in tokens if word not in string.punctuation]
    return tokens

def bag_of_words(text, vocab):
    """
    This function returns the bag of words of the user message.

    Parameters
    ----------
    text : string
        Text in input.

    vocab : list(string)
        Vocabulary of the model.

    Returns
    -------
    list(integer)
        The text in the bag of word format.
    """
    tokens = clean_text(text)
    bow = [0] * len(vocab)
    for word_1 in tokens:
        for idx, word_2 in enumerate(vocab):
            if word_2 == word_1:
                bow[idx] = 1
    # Return the bag of word of the sentence. We can use it to identify which words
    # are in the text, but we loose the information of the position ("Hello world" =>
    # [0,0,0,1,0,0,0,1,0,0] where 1 means that at this index in the vocab, the word is
    # "Hello" or "world").
    return np.array(bow)

def pred_class(text, vocab, labels):
    """
    This function compute the prediction with the user message.

    Parameters
    ----------
    text : string
        Text in input.

    vocab : list(string)
        Vocabulary of the model.
        
    labels : list(string)
        Classes in the model.

    Returns
    -------
    list(integer)
        The prediction done by the model.
    """
    bow = bag_of_words(text, vocab)
    prediction = MODEL.predict(np.array([bow]))

    # Get the proba of the sentence belong to a classes (if we have 3 classes, len(result) == 3).
    result = prediction[0]
    thresh = 0.5
    # Keep proba and classes if the proba >> thresh.
    y_pred = [[idx, res] for idx, res in enumerate(result) if res > thresh]

    # Sort the list on the proba, from the highest to the lowest.
    y_pred.sort(key=lambda x: x[1], reverse=True)

    test = [[CLASSES[idx], res] for idx, res in enumerate(result) if res > 0]
    test.sort(key=lambda x: x[1], reverse=True)

    print("PREDICTION:")
    for elem in test:
        print(elem)

    return_list = []

    for res in y_pred:
        return_list.append(labels[res[0]])

    return return_list

def get_response(intents_list, intents_json):
    """
    This function get the result for the prediction.

    Parameters
    ----------
    intents_list : list(string)
        List of the classes found for the prediction.

    intents_json : list(string)
        List of the intents object in the training file (intents_en.json).

    Returns
    -------
    string
        The response at the question.

    string
        The class of the prediction.
    """
    classe = ""
    if len(intents_list) == 0:  # If no result.
        result = "Sorry!"
        classe = "noanswer"
    else:
        tag = intents_list[0]  # Keep the best result.
        list_of_intents = intents_json["intents"]
        for i in list_of_intents:
            if i["tag"] == tag:
                classe = tag
                # Pick a random response in the json file.
                result = random.choice(i["reponses"])
                break
    return result, classe

def case_predict(message):
    """
    This function allow to find the type of prediction that the user want to do.

    Parameters
    ----------
    message : string
        Question of the user.

    Returns
    -------
    string
        The response at the question.

    string
        The link of the spring app, to launch the query of the user (can be empty).
    """
    find_label = False
    type_prediction = ""
    label_found = ""
    res = ""
    # Search if the user give a label.
    for word_1 in nltk.word_tokenize(message):
        for label in INFORMATION["label"]:
            if word_1.lower() == label.lower():
                # We have found a label, so now we search a property in the message which belong
                # the node with the label.
                label_found = label
                find_label = True
                # Search if the user give the property.
                for word_2 in nltk.word_tokenize(message):
                    for node_property in INFORMATION["regression"]:
                        # We have found a property, so we think that the user want to predict a
                        # property (regression) on a label (no ambiguity).
                        if word_2.lower() == node_property[1].lower() and \
                                node_property[0] == label_found:

                            type_prediction = "regression"
                            res = "I recommend you to use regression for the property " + \
                                node_property[1] + " of the node " + \
                                label_found + ". Click on the rocket to launch a regression " + \
                                "pipeline with the default parameters (on " + node_property[1] + \
                                " of the nodes " + label_found + ")."
                            return res, "http://localhost:8080/predictRegression?label=" + \
                                label_found + "&property=" + node_property[1] + \
                                 "&write=false&writeModel=false"

                    # We have found a label, so now we search a property in the message which
                    # belong the node with the label.
                    for node_property in INFORMATION["classification"]:
                        if word_2.lower() == node_property[1].lower() and \
                                node_property[0] == label_found:

                            type_prediction = "classification"
                            res = "I recommend you to use classification for the property " + \
                                node_property[1] + " of the node " + \
                                label_found + ". Click on the rocket to launch a " + \
                                    "classification pipeline with the default parameters" + \
                                    " (on " + node_property[1] + " of the nodes " + label_found + \
                                     ")."
                            return res, "http://localhost:8080/predictClassification?label=" + \
                                label_found + "&property=" + node_property[1] + \
                                "&write=false&writeModel=false"

        if find_label:
            # We have only find a label, maybe the user want to use a link prediction,
            # so we need to search an other label in the message.
            for word_2 in nltk.word_tokenize(message):
                for label in INFORMATION["label"]:
                    # We have found an other label, so we think that the user want to
                    # do a link prediction.
                    if word_2.lower() == label.lower() and label.lower() != label_found.lower():
                        type_prediction = "link prediction"
                        res = "I recommend you to use link prediction for this task (" + \
                            label_found + " " + label + "). Click on the rocket to launch " + \
                            "a link prediction pipeline with the default parameters (on the" + \
                            " path (" + label_found + ")-[]-(" + label + "))."
                        return res, "http://localhost:8080/predictLinkPrediction?path=" + \
                            label_found + "&path=" + label + "&typeResult=TOPN&" + \
                            "numberResult=&writeModel=false"
            res = "I found a label (" + label_found + \
                "), but I do not understand if you have also enter a property or an other " + \
                "label. Please check if your information are correct and retry."
            return res, ""

    if not find_label:
        property_found = []
        # There is no node label in the message, so we search if the user as just
        # enter the name of a property or a relation label.
        for word_1 in nltk.word_tokenize(message):  # We search a property.
            for label in INFORMATION["regression"]:
                # We have found a property, there is maybe ambiguity, but for now, we think
                # that the user want to predict (regression) the property.
                if word_1.lower() == label[1].lower():
                    type_prediction = "regression"
                    property_found.append((label[1], label[0]))
            for label in INFORMATION["classification"]:
                # We have found a property, there is maybe ambiguity, but for now, we think that
                # the user want to predict (classification) the property.
                if word_1.lower() == label[1].lower():
                    type_prediction = "classification"
                    property_found.append((label[1], label[0]))
        if len(property_found) != 0:
            return "I found several property with several label, please rewrite your question " + \
                "with more informations (" + str(property_found) + ").", ""
        if type_prediction == "":
            # We have not found any property, maybe the user want to use a link predicition with
            # only the label of the link.
            for word_1 in nltk.word_tokenize(message):
                for label in INFORMATION["linkPrediction"]:
                    # We have found a link label, so we think that the user want to use a
                    # link prediction.
                    if word_1.lower() == label[0].lower():
                        type_prediction = "link prediction"
                        res = "I recommend you to use link prediction for this task (" + \
                            label[0] + "). Click on the rocket to launch a link prediction " + \
                            "pipeline with the default parameters (on the path (" + \
                            label[1][0][0] + ")-[]-(" + label[1][1][0] + "))."
                        return res, "http://localhost:8080/predictLinkPrediction?path=" + \
                            label[1][0][0] + "&path=" + label[1][1][0] + \
                            "&typeResult=TOPN&writeModel=false"
    res = "I am sorry, I did not understand. Please check if your information are " + \
        "correct and retry."
    return res, ""

def case_display():
    """
    Case when the user want to display the graph.

    Returns
    -------
    string
        The response at the question.

    string
        The link of the spring app, to launch the query of the user.
    """
    return "Here the graph in your database!", \
        "http://localhost:8080/cypher?query=MATCH%20(n)-%5Br%20*0..1%5D-(m)%20RETURN%20DIST" + \
        "INCT%20n,r,m%20LIMIT%20500;"

def case_data_science_query(alogrithm):
    """
    Case when the user want to use an analytics algorithm on the graph.
    
    Parameters
    ----------
    alogrithm : string
        The algorithm found in the user's question.

    Returns
    -------
    string
        The response at the question.

    string
        The link of the spring app, to launch the query of the user.
    """
    if alogrithm in ["pageRank", "articleRank", "eigenvector", "betweenness", "degree"]:
        return "Here the result of " + alogrithm +" on the graph.", \
            "http://localhost:8080/centrality?algorithm=" + alogrithm + "&scale=true"

    if alogrithm in ["louvain", "labelPropagation"]:
        return "Here the result of " + alogrithm +" on the graph.", \
            "http://localhost:8080/communityDetection?algorithm=" + alogrithm

    if alogrithm in ["nodeSimilarity"]:
        return "Here the result of " + alogrithm +" on the graph.", \
            "http://localhost:8080/similarity?algorithm=" + alogrithm 

def case_most_important(message):
    """
    Case when the user want to use centrality.
    
    Parameters
    ----------
    message : string
        The user's question.

    Returns
    -------
    string
        The response at the question.

    string
        The link of the spring app, to launch the query of the user.
    """
    for word_1 in nltk.word_tokenize(message):
        for label in INFORMATION["label"]:
            if word_1.lower() == label.lower():
                return "Here the " + label + " most important (I used PageRank, you can " + \
                    "change the algoritmh if you want).", "http://localhost:8080/" + \
                    "cypher?scale=true&query=CALL%20easiGDS.pageRank()%20YIELD%20nodeId,score" + \
                    "%20WITH%20gds.util.asNode(nodeId)%20AS%20n,score%20AS%20score%20WHERE%20'" + \
                    label + "'%20IN%20labels(n)%20RETURN%20n,score%20ORDER%20BY%20score%20DESC"
    return "I am sorry, I did'nt find any label in the request, please check the informations.", ""

def reload_informations():
    """
    Allow to reload the information in the active neo4j database.
    """
    with open("./information.json", encoding="utf-8") as information_file:
        infomation_database_neo4j = information_file.read()

    #Informations get from the neo4j database.
    global INFORMATION
    INFORMATION = json.loads(infomation_database_neo4j)

def ask(message):
    """
    This function launch the process of prediction for a message.
        
    Parameters
    ----------
    message : string
        The user's question.

    Returns
    -------
    string
        The response at the question.

    string
        The link of the spring app, to launch the query of the user (can be empty).
    """

    intents = pred_class(message, WORDS, CLASSES)
    result, classe = get_response(intents, DATA)
    # print("CLASSE " + classe)
    if classe == "name":
        # Get the name of the user (the sentence must be in lower, except the name, it must be in
        # uppercase for at least the first letter).
        global NAME
        NAME = [word for word in nltk.word_tokenize(
            message[1:]) if word[0] in list(string.ascii_uppercase)]
        res = result
        for name in NAME:
            res += " " + name
        return res, ""
    if classe in ["predict", "template link prediction", "template classification regression v2",
                  "template classification regression v1"]:
        res, link = case_predict(message)
        return res, link

    if classe in ["display graph"]:
        res, link = case_display()
        return res, link

    if classe in ["pageRank", "articleRank", "eigenvector", "betweenness", "degree", "louvain", \
                  "labelPropagation", "nodeSimilarity"]:
        res, link = case_data_science_query(classe)
        return res, link

    if classe == "most important":
        res, link = case_most_important(message)
        return res, link

    if classe == "schema":
        return "Click on the rocket to see the schema of the active neo4j database.", \
            "http://localhost:8080/cypher?query=CALL%20db.schema.visualization()"

    if classe == "reload":
        return "Click on the rocket to reload the database in the chatbot.", \
            "http://localhost:8080/reload"

    return result, ""
