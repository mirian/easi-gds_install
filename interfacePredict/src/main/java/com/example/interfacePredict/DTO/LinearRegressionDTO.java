package com.example.interfacePredict.DTO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This is a DTO for the linear regression model used in the web. It implements ModelML.
 */
public class LinearRegressionDTO implements ModelML{
    private Integer batchSizeValue;
    private Integer batchSizeRangeBegin;
    private Integer batchSizeRangeEnd;
    private Integer minEpochsValue;
    private Integer minEpochsRangeBegin;
    private Integer minEpochsRangeEnd;
    private Integer maxEpochsValue;
    private Integer maxEpochsRangeBegin;
    private Integer maxEpochsRangeEnd;
    private Float learningRateValue;
    private Float learningRateRangeBegin;
    private Float learningRateRangeEnd;
    private Integer patienceValue;
    private Integer patienceRangeBegin;
    private Integer patienceRangeEnd;
    private Float toleranceValue;
    private Float toleranceRangeBegin;
    private Float toleranceRangeEnd;
    private Float penaltyValue;
    private Float penaltyRangeBegin;
    private Float penaltyRangeEnd;

    /**
     * Getter of the reel value of batchSize, choice between the range and the value.
     * @return Object (Integer or Map<String, list<Integer>>).
     */
    public Object getBatchSize(){
        if(this.batchSizeValue != null){
            return this.batchSizeValue;
        }
        if(this.batchSizeRangeBegin != null && this.batchSizeRangeEnd != null){
            Map<String, List<Integer>> map = new HashMap();
            List<Integer> list = new ArrayList<>();
            list.add(this.batchSizeRangeBegin);
            list.add(this.batchSizeRangeEnd);
            map.put("range", list);
            return map;
        }
        return null;
    }

    /**
     * Getter of the reel value of minEpochs, choice between the range and the value.
     * @return Object (Integer or Map<String, list<Integer>>).
     */
    public Object getMinEpochs(){
        if(this.minEpochsValue != null){
            return this.minEpochsValue;
        }
        if(this.minEpochsRangeBegin != null && this.minEpochsRangeEnd != null){
            Map<String, List<Integer>> map = new HashMap();
            List<Integer> list = new ArrayList<>();
            list.add(this.minEpochsRangeBegin);
            list.add(this.minEpochsRangeEnd);
            map.put("range", list);
            return map;
        }
        return null;
    }

    /**
     * Getter of the reel value of maxEpochs, choice between the range and the value.
     * @return Object (Integer or Map<String, list<Integer>>).
     */
    public Object getMaxEpochs(){
        if(this.maxEpochsValue != null){
            return this.maxEpochsValue;
        }
        if(this.maxEpochsRangeBegin != null && this.maxEpochsRangeEnd != null){
            Map<String, List<Integer>> map = new HashMap();
            List<Integer> list = new ArrayList<>();
            list.add(this.maxEpochsRangeBegin);
            list.add(this.maxEpochsRangeEnd);
            map.put("range", list);
            return map;
        }
        return null;
    }

    /**
     * Getter of the reel value of learningRate, choice between the range and the value.
     * @return Object (Float or Map<String, list<Float>>).
     */
    public Object getLearningRate(){
        if(this.learningRateValue != null){
            return this.learningRateValue;
        }
        if(this.learningRateRangeBegin != null && this.learningRateRangeEnd != null){
            Map<String, List<Float>> map = new HashMap();
            List<Float> list = new ArrayList<>();
            list.add(this.learningRateRangeBegin);
            list.add(this.learningRateRangeEnd);
            map.put("range", list);
            return map;
        }
        return null;
    }

    /**
     * Getter of the reel value of patience, choice between the range and the value.
     * @return Object (Integer or Map<String, list<Integer>>).
     */
    public Object getPatience(){
        if(this.patienceValue != null){
            return this.patienceValue;
        }
        if(this.patienceRangeBegin != null && this.patienceRangeEnd != null){
            Map<String, List<Integer>> map = new HashMap();
            List<Integer> list = new ArrayList<>();
            list.add(this.patienceRangeBegin);
            list.add(this.patienceRangeEnd);
            map.put("range", list);
            return map;
        }
        return null;
    }

    /**
     * Getter of the reel value of tolerance, choice between the range and the value.
     * @return Object (Float or Map<String, list<Float>>).
     */
    public Object getTolerance(){
        if(this.toleranceValue != null){
            return this.toleranceValue;
        }
        if(this.toleranceRangeBegin!= null && this.toleranceRangeEnd != null){
            Map<String, List<Float>> map = new HashMap();
            List<Float> list = new ArrayList<>();
            list.add(toleranceRangeBegin);
            list.add(toleranceRangeEnd);
            map.put("range", list);
            return map;
        }
        return null;
    }

    /**
     * Getter of the reel value of penalty, choice between the range and the value.
     * @return Object (Float or Map<String, list<Float>>).
     */
    public Object getPenalty(){
        if(this.penaltyValue != null){
            return this.penaltyValue;
        }
        if(this.penaltyRangeBegin != null && this.penaltyRangeEnd != null){
            Map<String, List<Float>> map = new HashMap();
            List<Float> list = new ArrayList<>();
            list.add(penaltyRangeBegin);
            list.add(penaltyRangeEnd);
            map.put("range", list);
            return map;
        }
        return null;
    }

    /**
     * Setter of the batchSizeValue.
     * @param batchSizeValue type of Integer.
     */
    public void setBatchSizeValue(Integer batchSizeValue) {
        this.batchSizeValue = batchSizeValue;
    }

    /**
     * Setter of the batchSizeRangeBegin.
     * @param batchSizeRangeBegin type of Integer.
     */
    public void setBatchSizeRangeBegin(Integer batchSizeRangeBegin) {
        this.batchSizeRangeBegin = batchSizeRangeBegin;
    }

    /**
     * Setter of the batchSizeRangeEnd.
     * @param batchSizeRangeEnd type of Integer.
     */
    public void setBatchSizeRangeEnd(Integer batchSizeRangeEnd) {
        this.batchSizeRangeEnd = batchSizeRangeEnd;
    }

    /**
     * Setter of the minEpochsValue.
     * @param minEpochsValue type of Integer.
     */
    public void setMinEpochsValue(Integer minEpochsValue) {
        this.minEpochsValue = minEpochsValue;
    }

    /**
     * Setter of the minEpochsRangeBegin.
     * @param minEpochsRangeBegin type of Integer.
     */
    public void setMinEpochsRangeBegin(Integer minEpochsRangeBegin) {
        this.minEpochsRangeBegin = minEpochsRangeBegin;
    }

    /**
     * Setter of the minEpochsRangeEnd.
     * @param minEpochsRangeEnd type of Integer.
     */
    public void setMinEpochsRangeEnd(Integer minEpochsRangeEnd) {
        this.minEpochsRangeEnd = minEpochsRangeEnd;
    }

    /**
     * Setter of the maxEpochsValue.
     * @param maxEpochsValue type of Integer.
     */
    public void setMaxEpochsValue(Integer maxEpochsValue) {
        this.maxEpochsValue = maxEpochsValue;
    }

    /**
     * Setter of the maxEpochsRangeBegin.
     * @param maxEpochsRangeBegin type of Integer.
     */
    public void setMaxEpochsRangeBegin(Integer maxEpochsRangeBegin) {
        this.maxEpochsRangeBegin = maxEpochsRangeBegin;
    }

    /**
     * Setter of the maxEpochsRangeEnd.
     * @param maxEpochsRangeEnd type of Integer.
     */
    public void setMaxEpochsRangeEnd(Integer maxEpochsRangeEnd) {
        this.maxEpochsRangeEnd = maxEpochsRangeEnd;
    }

    /**
     * Setter of the learningRateValue.
     * @param learningRateValue type of Float.
     */
    public void setLearningRateValue(Float learningRateValue) {
        this.learningRateValue = learningRateValue;
    }

    /**
     * Setter of the learningRateRangeBegin.
     * @param learningRateRangeBegin type of Float.
     */
    public void setLearningRateRangeBegin(Float learningRateRangeBegin) {
        this.learningRateRangeBegin = learningRateRangeBegin;
    }

    /**
     * Setter of the learningRateRangeEnd.
     * @param learningRateRangeEnd type of Float.
     */
    public void setLearningRateRangeEnd(Float learningRateRangeEnd) {
        this.learningRateRangeEnd = learningRateRangeEnd;
    }

    /**
     * Setter of the patienceValue.
     * @param patienceValue type of Integer.
     */
    public void setPatienceValue(Integer patienceValue) {
        this.patienceValue = patienceValue;
    }

    /**
     * Setter of the patienceRangeBegin.
     * @param patienceRangeBegin type of Integer.
     */
    public void setPatienceRangeBegin(Integer patienceRangeBegin) {
        this.patienceRangeBegin = patienceRangeBegin;
    }

    /**
     * Setter of the patienceRangeEnd.
     * @param patienceRangeEnd type of Integer.
     */
    public void setPatienceRangeEnd(Integer patienceRangeEnd) {
        this.patienceRangeEnd = patienceRangeEnd;
    }

    /**
     * Setter of the toleranceValue.
     * @param toleranceValue type of Float.
     */
    public void setToleranceValue(Float toleranceValue) {
        this.toleranceValue = toleranceValue;
    }

    /**
     * Setter of the toleranceRangeBegin.
     * @param toleranceRangeBegin type of Float.
     */
    public void setToleranceRangeBegin(Float toleranceRangeBegin) {
        this.toleranceRangeBegin = toleranceRangeBegin;
    }

    /**
     * Setter of the toleranceRangeEnd.
     * @param toleranceRangeEnd type of Float.
     */
    public void setToleranceRangeEnd(Float toleranceRangeEnd) {
        this.toleranceRangeEnd = toleranceRangeEnd;
    }

    /**
     * Setter of the penaltyValue.
     * @param penaltyValue type of Float.
     */
    public void setPenaltyValue(Float penaltyValue) {
        this.penaltyValue = penaltyValue;
    }

    /**
     * Setter of the penaltyRangeBegin.
     * @param penaltyRangeBegin type of Float.
     */
    public void setPenaltyRangeBegin(Float penaltyRangeBegin) {
        this.penaltyRangeBegin = penaltyRangeBegin;
    }

    /**
     * Setter of the penaltyRangeEnd.
     * @param penaltyRangeEnd type of Float.
     */
    public void setPenaltyRangeEnd(Float penaltyRangeEnd) {
        this.penaltyRangeEnd = penaltyRangeEnd;
    }

    /**
     * Setter of the batchSizeValue.
     * @return Integer.
     */
    public Integer getBatchSizeValue() {
        return this.batchSizeValue;
    }

    /**
     * Getter of the batchSizeRangeBegin.
     * @return Integer.
     */
    public Integer getBatchSizeRangeBegin() {
        return this.batchSizeRangeBegin;
    }

    /**
     * Getter of the batchSizeRangeEnd.
     * @return Integer.
     */
    public Integer getBatchSizeRangeEnd() {
        return this.batchSizeRangeEnd;
    }

    /**
     * Getter of the minEpochsValue.
     * @return Integer.
     */
    public Integer getMinEpochsValue() {
        return this.minEpochsValue;
    }

    /**
     * Getter of the minEpochsRangeBegin.
     * @return Integer.
     */
    public Integer getMinEpochsRangeBegin() {
        return this.minEpochsRangeBegin;
    }

    /**
     * Getter of the minEpochsRangeEnd.
     * @return Integer.
     */
    public Integer getMinEpochsRangeEnd() {
        return this.minEpochsRangeEnd;
    }

    /**
     * Getter of the maxEpochsValue.
     * @return Integer.
     */
    public Integer getMaxEpochsValue() {
        return this.maxEpochsValue;
    }

    /**
     * Getter of the maxEpochsRangeBegin.
     * @return Integer.
     */
    public Integer getMaxEpochsRangeBegin() {
        return this.maxEpochsRangeBegin;
    }

    /**
     * Getter of the maxEpochsRangeEnd.
     * @return Integer.
     */
    public Integer getMaxEpochsRangeEnd() {
        return this.maxEpochsRangeEnd;
    }

    /**
     * Getter of the learningRateValue.
     * @return Float.
     */
    public Float getLearningRateValue() {
        return this.learningRateValue;
    }

    /**
     * Getter of the learningRateRangeBegin.
     * @return Float.
     */
    public Float getLearningRateRangeBegin() {
        return this.learningRateRangeBegin;
    }

    /**
     * Getter of the learningRateRangeEnd.
     * @return Float.
     */
    public Float getLearningRateRangeEnd() {
        return this.learningRateRangeEnd;
    }

    /**
     * Getter of the patienceValue.
     * @return Float.
     */
    public Integer getPatienceValue() {
        return this.patienceValue;
    }

    /**
     * Getter of the patienceRangeBegin.
     * @return Float.
     */
    public Integer getPatienceRangeBegin() {
        return this.patienceRangeBegin;
    }

    /**
     * Getter of the patienceRangeEnd.
     * @return Float.
     */
    public Integer getPatienceRangeEnd() {
        return this.patienceRangeEnd;
    }

    /**
     * Getter of the toleranceValue.
     * @return Float.
     */
    public Float getToleranceValue() {
        return this.toleranceValue;
    }

    /**
     * Getter of the toleranceRangeBegin.
     * @return Float.
     */
    public Float getToleranceRangeBegin() {
        return this.toleranceRangeBegin;
    }

    /**
     * Getter of the toleranceRangeEnd.
     * @return Float.
     */
    public Float getToleranceRangeEnd() {
        return this.toleranceRangeEnd;
    }

    /**
     * Getter of the penaltyValue.
     * @return Float.
     */
    public Float getPenaltyValue() {
        return this.penaltyValue;
    }

    /**
     * Getter of the penaltyRangeBegin.
     * @return Float.
     */
    public Float getPenaltyRangeBegin() {
        return this.penaltyRangeBegin;
    }

    /**
     * Getter of the penaltyRangeEnd.
     * @return Float.
     */
    public Float getPenaltyRangeEnd() {
        return this.penaltyRangeEnd;
    }

    /**
     * Return True if the model contain value.
     * @return boolean.
     */
    public boolean notEmpty(){
        if (getBatchSize() != null){
            return true;
        }
        if (getMinEpochs() != null){
            return true;
        }
        if (getMaxEpochs() != null){
            return true;
        }
        if (getLearningRate() != null){
            return true;
        }
        if (getPatience() != null){
            return true;
        }
        if (getTolerance() != null){
            return true;
        }
        if (getPenalty() != null){
            return true;
        }
        return false;
    }

    /**
     * To string of the class.
     * @return String.
     */
    @Override
    public String toString() {
        String res = "{";
        if (getBatchSize() != null){
            res += "batchSize: " + getBatchSize() + ",";
        }
        if (getMinEpochs() != null){
            res += "minEpochs: " + getMinEpochs() + ",";
        }
        if (getMaxEpochs() != null){
            res += "maxEpochs: " + getMaxEpochs() + ",";
        }
        if (getLearningRate() != null){
            res += "learningRate: " + getLearningRate() + ",";
        }
        if (getPatience() != null){
            res += "patience: " + getPatience() + ",";
        }
        if (getTolerance() != null){
            res += "tolerance: " + getTolerance() + ",";
        }
        if (getPenalty() != null){
            res += "penalty: " + getPenalty() + ",";
        }
        if(res.charAt(res.length() - 1) == (',')){
            res = res.substring(0, res.length() - 1);
        }
        res = res.replace("=",":");
        res += "}";
        return res;
    }
}
