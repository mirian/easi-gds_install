package com.example.interfacePredict.DTO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This is a DTO for the random forest model used in the web. It implements ModelML.
 */
public class RandomForestDTO implements ModelML {
    private Float maxFeaturesRatioValue;
    private Float maxFeaturesRatioRangeBegin;
    private Float maxFeaturesRatioRangeEnd;
    private Float numberOfSamplesRatioValue;
    private Float numberOfSamplesRatioRangeBegin;
    private Float numberOfSamplesRatioRangeEnd;
    private Integer numberOfDecisionTreesValue;
    private Integer numberOfDecisionTreesRangeBegin;
    private Integer numberOfDecisionTreesRangeEnd;
    private Integer maxDepthValue;
    private Integer maxDepthRangeBegin;
    private Integer maxDepthRangeEnd;
    private Integer minLeafSizeValue;
    private Integer minLeafSizeRangeBegin;
    private Integer minLeafSizeRangeEnd;
    private Integer minSplitSizeValue;
    private Integer minSplitSizeRangeBegin;
    private Integer minSplitSizeRangeEnd;
    private String criterion;

    /**
     * Getter of the reel value of maxFeaturesRatio, choice between the range and the value.
     * @return Object (Float or Map<String, list<Float>>).
     */
    public Object getMaxFeaturesRatio(){
        if(this.maxFeaturesRatioValue != null){
            return this.maxFeaturesRatioValue;
        }
        if(this.maxFeaturesRatioRangeBegin != null && this.maxFeaturesRatioRangeEnd != null){
            Map<String, List<Float>> map = new HashMap();
            List<Float> list = new ArrayList<>();
            list.add(maxFeaturesRatioRangeBegin);
            list.add(maxFeaturesRatioRangeEnd);
            map.put("range", list);
            return map;
        }
        return null;
    }

    /**
     * Getter of the reel value of numberOfSamplesRatio, choice between the range and the value.
     * @return Object (Float or Map<String, list<Float>>).
     */
    public Object getNumberOfSamplesRatio(){
        if(this.numberOfSamplesRatioValue != null){
            return this.numberOfSamplesRatioValue;
        }
        if(this.numberOfSamplesRatioRangeBegin != null && this.numberOfSamplesRatioRangeEnd != null){
            Map<String, List<Float>> map = new HashMap();
            List<Float> list = new ArrayList<>();
            list.add(numberOfSamplesRatioRangeBegin);
            list.add(numberOfSamplesRatioRangeEnd);
            map.put("range", list);
            return map;
        }
        return null;
    }

    /**
     * Getter of the reel value of numberOfDecisionTrees, choice between the range and the value.
     * @return Object (Integer or Map<String, list<Integer>>).
     */
    public Object getNumberOfDecisionTrees(){
        if(this.numberOfDecisionTreesValue != null){
            return this.numberOfDecisionTreesValue;
        }
        if(this.numberOfDecisionTreesRangeBegin != null && this.numberOfDecisionTreesRangeEnd != null){
            Map<String, List<Integer>> map = new HashMap();
            List<Integer> list = new ArrayList<>();
            list.add(numberOfDecisionTreesRangeBegin);
            list.add(numberOfDecisionTreesRangeEnd);
            map.put("range", list);
            return map;
        }
        return null;
    }

    /**
     * Getter of the reel value of maxDepth, choice between the range and the value.
     * @return Object (Integer or Map<String, list<Integer>>).
     */
    public Object getMaxDepth(){
        if(this.maxDepthValue != null){
            return this.maxDepthValue;
        }
        if(this.maxDepthRangeBegin != null && this.maxDepthRangeEnd != null){
            Map<String, List<Integer>> map = new HashMap();
            List<Integer> list = new ArrayList<>();
            list.add(maxDepthRangeBegin);
            list.add(maxDepthRangeEnd);
            map.put("range", list);
            return map;
        }
        return null;
    }

    /**
     * Getter of the reel value of minLeafSize, choice between the range and the value.
     * @return Object (Integer or Map<String, list<Integer>>).
     */
    public Object getMinLeafSize(){
        if(this.minLeafSizeValue != null){
            return this.minLeafSizeValue;
        }
        if(this.minLeafSizeRangeBegin != null && this.minLeafSizeRangeEnd != null){
            Map<String, List<Integer>> map = new HashMap();
            List<Integer> list = new ArrayList<>();
            list.add(minLeafSizeRangeBegin);
            list.add(minLeafSizeRangeEnd);
            map.put("range", list);
            return map;
        }
        return null;
    }

    /**
     * Getter of the reel value of minSplitSize, choice between the range and the value.
     * @return Object (Integer or Map<String, list<Integer>>).
     */
    public Object getMinSplitSize(){
        if(this.minSplitSizeValue != null){
            return this.minSplitSizeValue;
        }
        if(this.minSplitSizeRangeBegin != null && this.minSplitSizeRangeEnd != null){
            Map<String, List<Integer>> map = new HashMap();
            List<Integer> list = new ArrayList<>();
            list.add(minSplitSizeRangeBegin);
            list.add(minSplitSizeRangeEnd);
            map.put("range", list);
            return map;
        }
        return null;
    }

    /**
     * Setter of the maxFeaturesRatioValue.
     * @param maxFeaturesRatioValue type of Float.
     */
    public void setMaxFeaturesRatioValue(Float maxFeaturesRatioValue) {
        this.maxFeaturesRatioValue = maxFeaturesRatioValue;
    }

    /**
     * Setter of the maxFeaturesRatioRangeBegin.
     * @param maxFeaturesRatioRangeBegin type of Float.
     */
    public void setMaxFeaturesRatioRangeBegin(Float maxFeaturesRatioRangeBegin) {
        this.maxFeaturesRatioRangeBegin = maxFeaturesRatioRangeBegin;
    }

    /**
     * Setter of the maxFeaturesRatioRangeEnd.
     * @param maxFeaturesRatioRangeEnd type of Float.
     */
    public void setMaxFeaturesRatioRangeEnd(Float maxFeaturesRatioRangeEnd) {
        this.maxFeaturesRatioRangeEnd = maxFeaturesRatioRangeEnd;
    }

    /**
     * Setter of the numberOfSamplesRatioValue.
     * @param numberOfSamplesRatioValue type of Float.
     */
    public void setNumberOfSamplesRatioValue(Float numberOfSamplesRatioValue) {
        this.numberOfSamplesRatioValue = numberOfSamplesRatioValue;
    }

    /**
     * Setter of the numberOfSamplesRatioValueRangeBegin.
     * @param numberOfSamplesRatioValueRangeBegin type of Float.
     */
    public void setNumberOfSamplesRatioRangeBegin(Float numberOfSamplesRatioValueRangeBegin) {
        this.numberOfSamplesRatioRangeBegin = numberOfSamplesRatioValueRangeBegin;
    }

    /**
     * Setter of the numberOfSamplesRatioValueRangeEnd.
     * @param numberOfSamplesRatioValueRangeEnd type of Float.
     */
    public void setNumberOfSamplesRatioRangeEnd(Float numberOfSamplesRatioValueRangeEnd) {
        this.numberOfSamplesRatioRangeEnd = numberOfSamplesRatioValueRangeEnd;
    }

    /**
     * Setter of the numberOfDecisionTreesValue.
     * @param numberOfDecisionTreesValue type of Integer.
     */
    public void setNumberOfDecisionTreesValue(Integer numberOfDecisionTreesValue) {
        this.numberOfDecisionTreesValue = numberOfDecisionTreesValue;
    }

    /**
     * Setter of the numberOfDecisionTreesValueRangeBegin.
     * @param numberOfDecisionTreesValueRangeBegin type of Integer.
     */
    public void setNumberOfDecisionTreesRangeBegin(Integer numberOfDecisionTreesValueRangeBegin) {
        this.numberOfDecisionTreesRangeBegin = numberOfDecisionTreesValueRangeBegin;
    }

    /**
     * Setter of the numberOfDecisionTreesValueRangeEnd.
     * @param numberOfDecisionTreesValueRangeEnd type of Integer.
     */
    public void setNumberOfDecisionTreesRangeEnd(Integer numberOfDecisionTreesValueRangeEnd) {
        this.numberOfDecisionTreesRangeEnd = numberOfDecisionTreesValueRangeEnd;
    }

    /**
     * Setter of the maxDepthValue.
     * @param maxDepthValue type of Integer.
     */
    public void setMaxDepthValue(Integer maxDepthValue) {
        this.maxDepthValue = maxDepthValue;
    }

    /**
     * Setter of the maxDepthValueRangeBegin.
     * @param maxDepthValueRangeBegin type of Integer.
     */
    public void setMaxDepthRangeBegin(Integer maxDepthValueRangeBegin) {
        this.maxDepthRangeBegin = maxDepthValueRangeBegin;
    }

    /**
     * Setter of the maxDepthValueRangeEnd.
     * @param maxDepthValueRangeEnd type of Integer.
     */
    public void setMaxDepthRangeEnd(Integer maxDepthValueRangeEnd) {
        this.maxDepthRangeEnd = maxDepthValueRangeEnd;
    }

    /**
     * Setter of the minLeafSizeValue.
     * @param minLeafSizeValue type of Integer.
     */
    public void setMinLeafSizeValue(Integer minLeafSizeValue) {
        this.minLeafSizeValue = minLeafSizeValue;
    }

    /**
     * Setter of the minLeafSizeValueRangeBegin.
     * @param minLeafSizeValueRangeBegin type of Integer.
     */
    public void setMinLeafSizeRangeBegin(Integer minLeafSizeValueRangeBegin) {
        this.minLeafSizeRangeBegin = minLeafSizeValueRangeBegin;
    }

    /**
     * Setter of the minLeafSizeValueRangeEnd.
     * @param minLeafSizeValueRangeEnd type of Integer.
     */
    public void setMinLeafSizeRangeEnd(Integer minLeafSizeValueRangeEnd) {
        this.minLeafSizeRangeEnd = minLeafSizeValueRangeEnd;
    }

    /**
     * Setter of the minSplitSizeValue.
     * @param minSplitSizeValue type of Integer.
     */
    public void setMinSplitSizeValue(Integer minSplitSizeValue) {
        this.minSplitSizeValue = minSplitSizeValue;
    }

    /**
     * Setter of the minSplitSizeValueRangeBegin.
     * @param minSplitSizeValueRangeBegin type of Integer.
     */
    public void setMinSplitSizeRangeBegin(Integer minSplitSizeValueRangeBegin) {
        this.minSplitSizeRangeBegin = minSplitSizeValueRangeBegin;
    }

    /**
     * Setter of the minSplitSizeValueRangeEnd.
     * @param minSplitSizeValueRangeEnd type of Integer.
     */
    public void setMinSplitSizeRangeEnd(Integer minSplitSizeValueRangeEnd) {
        this.minSplitSizeRangeEnd = minSplitSizeValueRangeEnd;
    }

    /**
     * Setter of the criterion.
     * @param criterion type of String.
     */
    public void setCriterion(String criterion) {
        this.criterion = criterion;
    }

    /**
     * Getter of the maxFeaturesRatioValue.
     * @return Float.
     */
    public Float getMaxFeaturesRatioValue() {
        return maxFeaturesRatioValue;
    }

    /**
     * Getter of the maxFeaturesRatioRangeBegin.
     * @return Float.
     */
    public Float getMaxFeaturesRatioRangeBegin() {
        return maxFeaturesRatioRangeBegin;
    }

    /**
     * Getter of the maxFeaturesRatioRangeEnd.
     * @return Float.
     */
    public Float getMaxFeaturesRatioRangeEnd() {
        return maxFeaturesRatioRangeEnd;
    }

    /**
     * Getter of the numberOfSamplesRatioValue.
     * @return Float.
     */
    public Float getNumberOfSamplesRatioValue() {
        return numberOfSamplesRatioValue;
    }

    /**
     * Getter of the numberOfSamplesRatioRangeBegin.
     * @return Float.
     */
    public Float getNumberOfSamplesRatioRangeBegin() {
        return numberOfSamplesRatioRangeBegin;
    }

    /**
     * Getter of the numberOfSamplesRatioRangeEnd.
     * @return Float.
     */
    public Float getNumberOfSamplesRatioRangeEnd() {
        return numberOfSamplesRatioRangeEnd;
    }

    /**
     * Getter of the numberOfDecisionTreesValue.
     * @return Integer.
     */
    public Integer getNumberOfDecisionTreesValue() {
        return numberOfDecisionTreesValue;
    }

    /**
     * Getter of the numberOfDecisionTreesRangeBegin.
     * @return Integer.
     */
    public Integer getNumberOfDecisionTreesRangeBegin() {
        return numberOfDecisionTreesRangeBegin;
    }

    /**
     * Getter of the numberOfDecisionTreesRangeEnd.
     * @return Integer.
     */
    public Integer getNumberOfDecisionTreesRangeEnd() {
        return numberOfDecisionTreesRangeEnd;
    }

    /**
     * Getter of the maxDepthValue.
     * @return Integer.
     */
    public Integer getMaxDepthValue() {
        return maxDepthValue;
    }

    /**
     * Getter of the maxDepthRangeBegin.
     * @return Integer.
     */
    public Integer getMaxDepthRangeBegin() {
        return maxDepthRangeBegin;
    }

    /**
     * Getter of the maxDepthRangeEnd.
     * @return Integer.
     */
    public Integer getMaxDepthRangeEnd() {
        return maxDepthRangeEnd;
    }

    /**
     * Getter of the minLeafSizeValue.
     * @return Integer.
     */
    public Integer getMinLeafSizeValue() {
        return minLeafSizeValue;
    }

    /**
     * Getter of the minLeafSizeRangeBegin.
     * @return Integer.
     */
    public Integer getMinLeafSizeRangeBegin() {
        return minLeafSizeRangeBegin;
    }

    /**
     * Getter of the minLeafSizeRangeEnd.
     * @return Integer.
     */
    public Integer getMinLeafSizeRangeEnd() {
        return minLeafSizeRangeEnd;
    }

    /**
     * Getter of the minSplitSizeValue.
     * @return Integer.
     */
    public Integer getMinSplitSizeValue() {
        return minSplitSizeValue;
    }

    /**
     * Getter of the minSplitSizeRangeBegin.
     * @return Integer.
     */
    public Integer getMinSplitSizeRangeBegin() {
        return minSplitSizeRangeBegin;
    }

    /**
     * Getter of the minSplitSizeRangeEnd.
     * @return Integer.
     */
    public Integer getMinSplitSizeRangeEnd() {
        return minSplitSizeRangeEnd;
    }

    /**
     * Getter of the penaltyRangeEnd.
     * @return String.
     */
    public String getCriterion() {
        if(criterion == null || criterion.equals("default")){
            return null;
        }
        return criterion;
    }

    /**
     * Return True if the model contain value.
     * @return boolean.
     */
    public boolean notEmpty(){
        if (getMaxFeaturesRatio() != null){
            return true;
        }
        if (getNumberOfSamplesRatio() != null){
            return true;
        }
        if (getNumberOfDecisionTrees() != null){
            return true;
        }
        if (getMaxDepth() != null){
            return true;
        }
        if (getMinLeafSize() != null){
            return true;
        }
        if (getMinSplitSize() != null){
            return true;
        }
        if (getCriterion() != null){
            return true;
        }
        return false;
    }

    /**
     * To string of the class.
     * @return String.
     */
    @Override
    public String toString() {
        String res = "{";
        if (getMaxFeaturesRatio() != null){
            res += "maxFeaturesRatio: " + getMaxFeaturesRatio() + ",";
        }
        if (getNumberOfSamplesRatio() != null){
            res += "numberOfSamplesRatio: " + getNumberOfSamplesRatio() + ",";
        }
        if (getNumberOfDecisionTrees() != null){
            res += "numberOfDecisionTrees: " + getNumberOfDecisionTrees() + ",";
        }
        if (getMaxDepth() != null){
            res += "maxDepth: " + getMaxDepth() + ",";
        }
        if (getMinLeafSize() != null){
            res += "minLeafSize: " + getMinLeafSize() + ",";
        }
        if (getMinSplitSize() != null){
            res += "minSplitSize: " + getMinSplitSize() + ",";
        }
        if (getCriterion() != null){
            res += "criterion: \"" + getCriterion() + "\",";
        }
        if(res.charAt(res.length() - 1) == (',')){
            res = res.substring(0, res.length() - 1);
        }
        res = res.replace("=",":");
        res += "}";
        return res;
    }
}
