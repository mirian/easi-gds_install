package com.example.interfacePredict.DTO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * This class contains the path choosen by the user for a link prediction.
 */
public class PathDTO {
    private List<String> path;

    /**
     *Constructor of the class.
     */
    public PathDTO(){
        this.path = new ArrayList<>();
    }

    /**
     *Getter of path.
     * @return Collection<String>.
     */
    public Collection<String> getPath(){
        return this.path;
    }

    /**
     *Setter of path.
     * @param path type of List<String>.
     */
    public void setPath(List<String> path) {
        this.path = path;
    }

    /**
     * To string of the class.
     * @return String.
     */
    @Override
    public String toString(){
        String res = "[";
        for(String label : this.path){
            res += "\"" + label + "\",";
        }
        if(res.charAt(res.length() - 1) == (',')){
            res = res.substring(0, res.length() - 1);
        }
        res += "]";
        return res;
    }
}
