package com.example.interfacePredict.DTO;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class allows to stock all the configuration (random forest, MLP, ...) made by the user.
 */
@Component
@Scope("session")
public class ConfigML {
    private Map<String, List<ModelML>> models;

    /**
     * Constructor of the class.
     */
    public ConfigML(){
        this.models = new HashMap<>();
        this.models.put("RF", new ArrayList<>());
        this.models.put("MLP", new ArrayList<>());
        this.models.put("LR", new ArrayList<>());
    }

    /**
     * Setter of models.
     * @param models type of Map<String, List<ModelML>.
     */
    public void setModels(Map<String, List<ModelML>> models) {
        this.models = models;
    }

    /**
     * Getter of models.
     * @return models type of Map<String, List<ModelML>.
     */
    public Map<String, List<ModelML>> getModels() {
        return models;
    }

    /**
     * Add a random forest model to the list of models.
     * @param modelML type of ModelML.
     */
    public void addRF(ModelML modelML){
        this.models.get("RF").add(modelML);
    }

    /**
     * Add an MLP model to the list of models.
     * @param modelML type of ModelML.
     */
    public void addMLP(ModelML modelML){
        this.models.get("MLP").add(modelML);
    }

    /**
     * Add a logistic or linear regression model to the list of models.
     * @param modelML type of ModelML.
     */
    public void addLR(ModelML modelML){
        this.models.get("LR").add(modelML);
    }

    /**
     * Return true if there is no random forest in the list of models.
     * @return boolean.
     */
    public boolean getRFEmpty(){
        return this.models.get("RF").size() == 0;
    }

    /**
     * Return true if there is no MLP in the list of models.
     * @return boolean.
     */
    public boolean getMLPEmpty(){
        return this.models.get("MLP").size() == 0;
    }

    /**
     * Return true if there is no logistic or linear regression in the list of models.
     * @return boolean.
     */
    public boolean getLREmpty(){
        return this.models.get("LR").size() == 0;
    }

    /**
     * Return true if no models are in the list of models
     * @return boolean
     */
    public boolean getConfigEmpty(){
        return this.getLREmpty() && this.getRFEmpty() && this.getMLPEmpty();
    }
    /**
     * To string of the class.
     * @return String.
     */
    @Override
    public String toString(){
        String res = this.models.toString();
        res = res.replace("=",":");
        return res;
    }
}
