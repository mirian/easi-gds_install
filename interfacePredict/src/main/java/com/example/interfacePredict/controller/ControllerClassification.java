package com.example.interfacePredict.controller;

import com.example.interfacePredict.DTO.*;
import com.example.interfacePredict.services.Facade;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Controller used for the classification task.
 */
@org.springframework.stereotype.Controller
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ControllerClassification {
    @Autowired
    private Facade facade;

    /**
     * Function get of the url /classification
     * @param session of the type HttpSession.
     * @param model of the type Model.
     * @return String.
     */
    @GetMapping("/classification")
    public String classification(HttpSession session,
                                 Model model
    ){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        model.addAttribute("locale", LocaleContextHolder.getLocale().toString());
        model.addAttribute("metaTitle", "Classification");
        model.addAttribute("css", "classification");
        model.addAttribute("active", "classification");

        session.setAttribute("configML", new ConfigML());
        session.setAttribute("label", "");
        session.setAttribute("property", "");
        session.setAttribute("path", new ArrayList<String>());

        return "classification";
    }

    /**
     * Function get of the url /classificationModel
     * @param model of the type HttpSession.
     * @param session of the type Model.
     * @return String.
     */
    @GetMapping("/classificationModel")
    public String classificationModel(HttpSession session,
                                      Model model
    ){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        model.addAttribute("locale", LocaleContextHolder.getLocale().toString());
        model.addAttribute("metaTitle", "Classification");
        model.addAttribute("css", "classification");
        model.addAttribute("active", "classification");

        return "classification";
    }

    /**
     * Function get of the url /predictClassification
     * @param session of the type HttpSession.
     * @return String.
     */
    @GetMapping("/predictClassification")
    public String displayClassification(HttpSession session){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }
        return "redirect:/classification";
    }

    /**
     * Function post of the url /predictClassification
     * @param label of the type String.
     * @param property of the type String.
     * @param write of the type String.
     * @param writeModel of the type String.
     * @param features of the type List<String>.
     * @param context of the type List<String>.
     * @param session of the type HttpSession.
     * @param model of the type Model.
     * @return String.
     */
    @CrossOrigin
    @PostMapping("/predictClassification")
    public String displayClassification(@RequestParam("label") String label,
                                        @RequestParam("property") String property,
                                        @RequestParam("write") String write,
                                        @RequestParam("writeModel") String writeModel,
                                        @RequestParam(value = "features", required=false) List<String> features,
                                        @RequestParam(value = "context", required=false) List<String> context,
                                        HttpSession session,
                                        Model model
    ){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        if(features == null){
            features = new ArrayList<>();
        }
        if(context == null){
            context = new ArrayList<>();
        }
        //At the beginning, we have one RF, MLP and LR with their default values.
        ConfigML configML1 = (ConfigML) session.getAttribute("configML");
        if(configML1.getRFEmpty() && configML1.getMLPEmpty() && configML1.getLREmpty()){
            configML1.addRF(new RandomForestDTO());
            configML1.addMLP(new MultiLinearPerceptronDTO());
            configML1.addLR(new LogisticRegression());
        }
        String query = "CALL easiGDS.templateNodeClass(\"" + label + "\",\""  + property + "\"," +
                facade.listToString(features) +","  + configML1 + ","  + write + "," +
                facade.listToString(context) + "," + writeModel + ") YIELD node as node,prediction " +
                "as prediction return id(node),node,prediction;";

        model.addAttribute("query", query);
        model.addAttribute("locale", LocaleContextHolder.getLocale().toString());
        model.addAttribute("metaTitle", "Classification");
        model.addAttribute("css", "resultsClassification");
        model.addAttribute("active", "classification");

        return "resultsClassification";
    }

    /**
     * Function get of the url /multiLinearPerceptron
     * @param label of the type String.
     * @param property of the type String.
     * @param session of the type HttpSession.
     * @param model of the type Model.
     * @return String.
     */
    @GetMapping("/multiLinearPerceptron")
    public String multiLinearPerceptron(@RequestParam("label") String label,
                                        @RequestParam("property") String property,
                                        HttpSession session,
                                        Model model
    ){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        MultiLinearPerceptronDTO multiLinearPerceptronDTO = new MultiLinearPerceptronDTO();

        model.addAttribute("multiLinearPerceptronDTO", multiLinearPerceptronDTO);
        model.addAttribute("locale", LocaleContextHolder.getLocale().toString());
        model.addAttribute("metaTitle", "Classification");
        model.addAttribute("css", "multiLinearPerceptron");
        model.addAttribute("active", "classification");

        session.setAttribute("label", label);
        session.setAttribute("property", property);

        return "multiLinearPerceptronClassification";
    }

    /**
     * Function post of the url /logisticRegression
     * @param logisticRegressionDTO of the type LogisticRegression.
     * @param session of the type HttpSession.
     * @return String.
     */
    @PostMapping("/logisticRegression")
    public String logisticRegression(@ModelAttribute("logisticRegressionDTO") LogisticRegression
                                                 logisticRegressionDTO,
                                     HttpSession session
    ){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }
        ConfigML configML1 = (ConfigML) session.getAttribute("configML");
        if(logisticRegressionDTO.notEmpty() || !configML1.getConfigEmpty()){
            configML1.addLR(logisticRegressionDTO);
        }
        session.setAttribute("configML", configML1);

        return "redirect:/classificationModel";
    }

    /**
     * Function get of the url /logisticRegression
     * @param label of the type String.
     * @param property of the type String.
     * @param session of the type HttpSession.
     * @param model of the type Model.
     * @return String.
     */
    @GetMapping("/logisticRegression")
    public String logisticRegression(@RequestParam("label") String label,
                                     @RequestParam("property") String property,
                                     HttpSession session,
                                     Model model
    ){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }
        LogisticRegression logisticRegressionDTO = new LogisticRegression();

        model.addAttribute("locale", LocaleContextHolder.getLocale().toString());
        model.addAttribute("metaTitle", "Classification");
        model.addAttribute("css", "logisticRegression");
        model.addAttribute("active", "classification");
        model.addAttribute("logisticRegressionDTO", logisticRegressionDTO);

        session.setAttribute("label", label);
        session.setAttribute("property", property);

        return "logisticRegressionClassification";
    }

    /**
     * Function post of the url /multiLinearPerceptron
     * @param multiLinearPerceptronDTO of the type MultiLinearPerceptronDTO.
     * @param session of the type HttpSession.
     * @return String.
     */
    @PostMapping("/multiLinearPerceptron")
    public String multiLinearPerceptron(@ModelAttribute("multiLinearPerceptronDTO") MultiLinearPerceptronDTO
                                                    multiLinearPerceptronDTO,
                                        HttpSession session
    ){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        ConfigML configML1 = (ConfigML) session.getAttribute("configML");
        if(multiLinearPerceptronDTO.notEmpty() || !configML1.getConfigEmpty()){
            configML1.addMLP(multiLinearPerceptronDTO);
        }
        session.setAttribute("configML", configML1);

        return "redirect:/classificationModel";
    }

    /**
     * Function get of the url /randomForest
     * @param label of the type String.
     * @param property of the type String.
     * @param session of the type HttpSession.
     * @param model of the type Model.
     * @return String.
     */
    @GetMapping("/randomForest")
    public String randomForest(@RequestParam("label") String label,
                               @RequestParam("property") String property,
                               HttpSession session,
                               Model model
    ){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        RandomForestDTO randomForestDTO = new RandomForestDTO();

        model.addAttribute("locale", LocaleContextHolder.getLocale().toString());
        model.addAttribute("metaTitle", "Classification");
        model.addAttribute("css", "randomForest");
        model.addAttribute("active", "classification");
        model.addAttribute("randomForestDTO", randomForestDTO);

        session.setAttribute("label", label);
        session.setAttribute("property", property);

        return "randomForestClassification";
    }

    /**
     * Function post of the url /randomForest
     * @param randomForestDTO of the type RandomForestDTO.
     * @param session of the type HttpSession.
     * @return String.
     */
    @PostMapping("/randomForest")
    public String randomForest(@ModelAttribute("randomForestDTO") RandomForestDTO randomForestDTO,
                               HttpSession session
    ){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }
        ConfigML configML1 = (ConfigML) session.getAttribute("configML");

        if(randomForestDTO.notEmpty() || !configML1.getConfigEmpty()){

            configML1.addRF(randomForestDTO);
        }
        session.setAttribute("configML", configML1);

        return "redirect:/classificationModel";
    }

    /**
     * Reset the models enter by the user to default value
     * @param label
     * @param property
     * @param session
     * @return
     */
    @GetMapping("/classificationModelsReset")
    public String classificationModelsReset(@RequestParam("label") String label,
                                            @RequestParam("property") String property,
                                            HttpSession session){

        session.setAttribute("configML", new ConfigML());
        session.setAttribute("label", label);
        session.setAttribute("property", property);

        return "redirect:/classificationModel";
    }
}
