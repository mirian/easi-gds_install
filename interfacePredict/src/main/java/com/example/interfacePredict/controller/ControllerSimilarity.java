package com.example.interfacePredict.controller;

import com.example.interfacePredict.services.Facade;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller used for the similarity algorithms.
 */
@org.springframework.stereotype.Controller
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ControllerSimilarity {
    @Autowired
    private Facade facade;

    /**
     * Function get for the url /similarity
     * @param session of the type HttpSession.
     * @param model of the type Model.
     * @return String.
     */
    @GetMapping("/similarity")
    public String similarity(HttpSession session,
                             Model model){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"), session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        if(LocaleContextHolder.getLocale().toString().equals("en")){
            model.addAttribute("metaTitle", "Similarity");
        }
        else{
            model.addAttribute("metaTitle", "Similarité");
        }
        model.addAttribute("locale", LocaleContextHolder.getLocale().toString());
        model.addAttribute("css", "similarity");
        model.addAttribute("active", "similarity");

        return "similarity";
    }

    /**
     * Function post of the url /similarity
     * @param algorithm of the type String.
     * @param session of the type HttpSession.
     * @param model of the type Model.
     * @return String.
     */
    @PostMapping("/similarity")
    public String resultSimilarity(@RequestParam("algorithm") String algorithm,
                                   HttpSession session,
                                   Model model){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"), session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        model.addAttribute("metaTitle", "Cypher");
        model.addAttribute("locale", LocaleContextHolder.getLocale().toString());
        model.addAttribute("css", "cypher");
        model.addAttribute("active", "cypher");

        String query = "CALL easiGDS." + algorithm + "() YIELD node1, node2, similarity RETURN gds.util.asNode(node1) AS " +
                "node1, gds.util.asNode(node2) AS node2, similarity ORDER BY similarity DESC;";
        model.addAttribute("query", query);

        return "cypher";
    }
}
