package com.example.interfacePredict.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.neo4j.driver.*;
import org.neo4j.driver.Record;
import org.neo4j.driver.types.MapAccessor;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

/**
 * This controller is used to connect our website with neo4j database
 */
@org.springframework.stereotype.Controller
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)

@CrossOrigin
public class ControllerNeo4JConnect {

    /**
     * Used to check if the value to connect give by the user are correct and retrieve the default neo4j database.
     * @param session
     * @param body
     * @return
     * @throws JsonProcessingException
     */
    @PostMapping("/driverNeo4jConnectLogin")
    public ResponseEntity driverNeo4jConnectLogin(
            HttpSession session,
            @RequestBody Map body
    ) throws JsonProcessingException {
        try {

            ObjectMapper o = new ObjectMapper();
            String cypher;


            ResourceBundle messages = ResourceBundle.getBundle("messages",LocaleContextHolder.getLocale());



            if(!body.containsKey("cypher")){
                var map = new HashMap<>();
                map.put("name",messages.getString("neo4j.exception.name"));
                map.put("message",messages.getString("neo4j.exception.missing.cypher"));

                String jsonMap = o.writeValueAsString(map);
                return ResponseEntity.status(500).body(jsonMap);
            }
            else {
                cypher = (String) body.get("cypher");
            }
            String userName,pwd,address;
                    if(body.get("userName").equals("")){
                        var map = new HashMap<>();
                        map.put("name",messages.getString("login.exception.name"));
                        map.put("message",messages.getString("login.exception.missing.username"));

                        String jsonMap = o.writeValueAsString(map);
                        return ResponseEntity.status(400).body(jsonMap);
                    }else{
                        userName = (String) body.get("userName");
                    }

                    if(body.get("pwd").equals("")){
                        var map = new HashMap<>();
                        map.put("name",messages.getString("login.exception.name"));

                        map.put("message",messages.getString("login.exception.missing.password"));

                        String jsonMap = o.writeValueAsString(map);
                        return ResponseEntity.status(400).body(jsonMap);
                    }else{
                        pwd = (String) body.get("pwd");
                    }

                    if(body.get("address").equals("")){
                        var map = new HashMap<>();
                        map.put("name",messages.getString("login.exception.name"));

                        map.put("message",messages.getString("login.exception.missing.address"));

                        String jsonMap = o.writeValueAsString(map);
                        return ResponseEntity.status(400).body(jsonMap);
                    }else{
                        address = (String) body.get("address");
                    }
                if(body.get("port").equals("")) {
                    var map = new HashMap<>();
                    map.put("name",messages.getString("login.exception.name"));

                    map.put("message",messages.getString("login.exception.missing.port"));

                    String jsonMap = o.writeValueAsString(map);
                    return ResponseEntity.status(400).body(jsonMap);

                }

                Driver driver = GraphDatabase.driver("neo4j://"+address, AuthTokens.basic(userName, pwd));
                session.setAttribute("driver",driver);

            Session sessionNeo4j;
            String db = (String) session.getAttribute("db");
            if(db==null){
                sessionNeo4j = driver.session();
            }
            else {
                sessionNeo4j = driver.session(SessionConfig.forDatabase(db));
            }
            var result = sessionNeo4j.run(cypher);

            var map = new HashMap<>();
            map.put("keys",result.keys());
            map.put("records",recordsToMap(result.stream().toList()));


            String jsonMap = null;
            try {
                jsonMap = o.writeValueAsString(map);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
            return ResponseEntity.ok(jsonMap);



        }
        catch (Exception e){
            ObjectMapper o = new ObjectMapper();
            var mapError = new HashMap<>();
            mapError.put("message",e.toString());
            return ResponseEntity.status(400).body(o.writeValueAsString(mapError));
        }
    }

    /**
     * Used to connect to neo4j via the java driver (for dynamic fields in Machine Learning)
     * @param session
     * @param body
     * @return
     * @throws JsonProcessingException
     */
    @PostMapping("/driverNeo4jConnect")
    public ResponseEntity driverNeo4jConnect(
            HttpSession session,
            @RequestBody Map body
    ) throws JsonProcessingException {
        try {
        ObjectMapper o = new ObjectMapper();
        ResourceBundle messages = ResourceBundle.getBundle("messages",LocaleContextHolder.getLocale());
        String cypher;
        if(!body.containsKey("cypher")){
            var map = new HashMap<>();
            map.put("name",messages.getString("neo4j.exception.name"));
            map.put("message",messages.getString("neo4j.exception.missing.cypher"));

            String jsonMap = o.writeValueAsString(map);
            return ResponseEntity.status(500).body(jsonMap);
        }
        else {
            cypher = (String) body.get("cypher");
        }

            Driver driver = (Driver) session.getAttribute("driver");

            Session sessionNeo4j;
            String db = (String) session.getAttribute("db");
            if(body.containsKey("db")){
                db = (String) body.get("db");
            }
            if(db==null){
                sessionNeo4j = driver.session();
            }
            else {
                sessionNeo4j = driver.session(SessionConfig.forDatabase(db));
            }
            var result = sessionNeo4j.run(cypher);

            var map = new HashMap<>();
            map.put("keys",result.keys());
            map.put("records",recordsToMap(result.stream().toList()));


                    String jsonMap = null;
                    try {
                        jsonMap = o.writeValueAsString(map);
                    } catch (JsonProcessingException e) {
                        throw new RuntimeException(e);
                    }
                    return ResponseEntity.ok(jsonMap);



        }
        catch (Exception e){
            ObjectMapper o = new ObjectMapper();
            var mapError = new HashMap<>();
            mapError.put("message",e.toString());
            return ResponseEntity.status(400).body(o.writeValueAsString(mapError));
        }
    }

    /**
     * Convert a list of Record in a List of Map
     * @param records
     * @return
     */
    public List<Map> recordsToMap(List<Record>records){
        ArrayList res = new ArrayList();
        for(Record r : records){
            res.add(r.asMap());
        }
        return res;
    }

    /**
     * Use to connect to neo4j database via the HTTP API (When we need to display a graph)
     * @param session
     * @param body
     * @return
     * @throws IOException
     */
    @PostMapping("/httpNeo4jRequest")
    public ResponseEntity httpNeo4jRequest(
            HttpSession session,
            @RequestBody Map body
            ) throws IOException {
        String cypher = (String) body.get("cypher");
        String address = (String) session.getAttribute("address");
        var port =  session.getAttribute("port");
        String db;
        if(body.containsKey("db")){
            db = (String) body.get("db");
        }
        else {
            db = (String) session.getAttribute("db");
        }

        URL url = new URL("http://"+address+':'+port+"/db/"+db+"/tx/commit");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

        String userName = (String) session.getAttribute("userName");
        String pwd = (String) session.getAttribute("pwd");
        String userCredentials = userName+":"+pwd;
        String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userCredentials.getBytes()));


        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty(  "Accept","application/json");
        urlConnection.setRequestProperty(  "Content-Type","application/json");
        urlConnection.setRequestProperty(  "Authorization",basicAuth);
        urlConnection.setDoOutput(true);

        String jsonBody = "{\"statements\":[{\"statement\":"+cypher+",\"resultDataContents\": [\"row\", \"graph\"]}]}";
        try(OutputStream os = urlConnection.getOutputStream()) {
            byte[] input = jsonBody.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        try(BufferedReader br = new BufferedReader(
                new InputStreamReader(urlConnection.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            return ResponseEntity.ok(response.toString());
        }



    }
}
