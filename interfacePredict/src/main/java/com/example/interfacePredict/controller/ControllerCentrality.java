package com.example.interfacePredict.controller;

import com.example.interfacePredict.services.Facade;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * The controller used for the centrality algorithms.
 */
@org.springframework.stereotype.Controller
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ControllerCentrality {
    @Autowired
    private Facade facade;

    /**
     * Function get for the url /centrality
     * @param session of the type HttpSession.
     * @param model of the type Model.
     * @return String.
     */
    @GetMapping("/centrality")
    public String centrality(HttpSession session,
                            Model model){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"), session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        if(LocaleContextHolder.getLocale().toString().equals("en")){
            model.addAttribute("metaTitle", "Centrality");
        }
        else{
            model.addAttribute("metaTitle", "Centralité");
        }
        model.addAttribute("locale", LocaleContextHolder.getLocale().toString());
        model.addAttribute("css", "centrality");
        model.addAttribute("active", "centrality");

        return "centrality";
    }

    /**
     * Function post for the url /centrality
     * @param algorithm of the type String.
     * @param scale of the type String.
     * @param session of the type HttpSession.
     * @param model of the type Model.
     * @return String.
     */
    @PostMapping("/centrality")
    public String resultCentrality(@RequestParam("algorithm") String algorithm,
                                   @RequestParam(value = "scale", defaultValue = "") String scale,
                                   HttpSession session,
                                   Model model){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        model.addAttribute("metaTitle", "Cypher");
        model.addAttribute("locale", LocaleContextHolder.getLocale().toString());
        model.addAttribute("css", "cypher");
        model.addAttribute("active", "cypher");

        String query = "CALL easiGDS." + algorithm + "() YIELD nodeId, score RETURN gds.util.asNode(nodeId) AS " +
                "node, score ORDER BY score DESC;";
        model.addAttribute("query", query);
        model.addAttribute("scale", scale);

        return "cypher";
    }
}
