package com.example.interfacePredict.controller;

import com.example.interfacePredict.DTO.ConfigML;
import com.example.interfacePredict.services.Facade;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Default controller of the website.
 */
@org.springframework.stereotype.Controller
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class Controller {
    @Autowired
    private Facade facade;

    /**
     * Function get of the url /
     * @param request of the type HttpServletRequest.
     * @param response of the type HttpServletResponse.
     * @param model of the type Model.
     * @return String.
     */
    @GetMapping("/")
    public String login(HttpServletRequest request,
                        HttpServletResponse response,
                        Model model){
        /*
        Get cookie, if there is no cookie with idUser, we create one. It is used for the
        rest service with the chatbot to keep the history of message.
         */
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            if(!Arrays.stream(cookies)
                    .map(c -> c.getName() + "=" + c.getValue()).collect(Collectors.joining(", "))
                    .contains("idUser")){
                Cookie cookie = new Cookie("idUser", facade.getLastIdUser().toString());
                response.addCookie(cookie);
            }
        }
        model.addAttribute("metaTitle", "Login");
        model.addAttribute("css", "login");
        model.addAttribute("active", "login");
        return "login";
    }

    /**
     * Function get/post of the url /index
     * @param userName of the type String.
     * @param pwd of the type String.
     * @param port of the type Integer.
     * @param session of the type HttpSession.
     * @return String.
     */
    @RequestMapping("/index")
    public String index(@RequestParam(value = "userName", required=false) String userName,
                        @RequestParam(value = "pwd", required=false) String pwd,
                        @RequestParam(value = "port", required=false) Integer port,
                        @RequestParam(value = "db", required = false) String db,
                        @RequestParam(value = "address", required = false) String address,
                        HttpSession session,
                        Model model
    ){
        /*
        Add in session the username, password and port.
        */
        if(userName != null){
            session.setAttribute("userName", userName);
        }
        if(pwd != null){
            session.setAttribute("pwd", pwd);
        }
        if(port != null){
            session.setAttribute("port", port);
        }
        if(db != null){
            if(session.getAttribute("db") == null) {
                session.setAttribute("db", db);
            }
        }
        if(address != null){
            session.setAttribute("address", address);
        }
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        //Store the models created by the user in session.
        session.setAttribute("configML", new ConfigML());

        model.addAttribute("locale", LocaleContextHolder.getLocale().toString());
        model.addAttribute("css", "index");
        model.addAttribute("active", "accueil");

        if(LocaleContextHolder.getLocale().toString().equals("en")){
            model.addAttribute("metaTitle", "Home");
        }
        else{
            model.addAttribute("metaTitle", "Accueil");
        }
        return "index";
    }

    /**
     * Function get/post on the url /cypher
     * @param query of the type String.
     * @param scale of the type Boolean.
     * @param session of the type HttpSession.
     * @param model of the type Model.
     * @return String.
     */
    @RequestMapping("/cypher")
    public String cypher(@RequestParam(value = "query", defaultValue = "") String query,
                         @RequestParam(value = "scale", defaultValue = "") String scale,
                         HttpSession session,
                         Model model
    ){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        model.addAttribute("locale", LocaleContextHolder.getLocale().toString());
        model.addAttribute("css", "cypher");
        model.addAttribute("active", "cypher");

        model.addAttribute("metaTitle", "Cypher");
        model.addAttribute("query", query);
        model.addAttribute("scale", scale);

        return "cypher";
    }

    /**
     * Set in session to check if the chatbox is open
     * @param chatBotState of type boolean.
     * @param session of type HttpSession.
     * @return ResponseEntity
     */
    @PostMapping("/chatBotState")
    public ResponseEntity setChatBotState(@RequestParam boolean chatBotState, HttpSession session){
        session.setAttribute("chatBotState",chatBotState);
        return ResponseEntity.ok().build();
    }

    /**
     * Get a value to check if the chatbox was open in previous page
     * @param session of type HttpSession.
     * @return ResponseEntity.
     */
    @GetMapping("/chatBotState")
    public ResponseEntity<?> getChatBotState(HttpSession session){

        Object state = session.getAttribute("chatBotState");
        if (state==null){
            return ResponseEntity.ok().header("state","false").build();
        }
        else{
            boolean response = (boolean) state;
            return ResponseEntity.ok().header("state",response+"").build();
        }
    }

    /**
     * Page to let user change the database on his neo4j DBMS
     * @param session of type HttpSession.
     * @param model of type Model.
     * @return String.
     */
    @GetMapping("/changeDB")
    public String getChangeDB(HttpSession session, Model model){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        model.addAttribute("locale", LocaleContextHolder.getLocale().toString());
        model.addAttribute("css", "changeDB");
        model.addAttribute("active", "changeDB");

        if(LocaleContextHolder.getLocale().toString().equals("en")){
            model.addAttribute("metaTitle", "Change Database");
        }
        else{
            model.addAttribute("metaTitle", "Changer de base de données");
        }

        return "changeDB";
    }

    /**
     * Set in session the name of the selected database
     * @param database of type String.
     * @param session of type HttpSession.
     * @param model of type Model.
     * @return String.
     */
    @GetMapping("/setDB")
    public String setChangeDB(@RequestParam String database, HttpSession session, Model model){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        session.setAttribute("db",database);

        model.addAttribute("locale", LocaleContextHolder.getLocale().toString());
        model.addAttribute("css", "changeDB");
        model.addAttribute("active", "changeDB");

        if(LocaleContextHolder.getLocale().toString().equals("en")){
            model.addAttribute("metaTitle", "Change Database");
        }
        else{
            model.addAttribute("metaTitle", "Changer de base de données");
        }

        return "redirect:/changeDB";
    }

    /**
     * Reload the information of the active neo4j database in the chatbot.
     * @param request of type HttpServletRequest.
     * @param session of type HttpSession.
     * @param model of type Model.
     * @return String.
     */
    @RequestMapping("/reload")
    public String reload(HttpServletRequest request,
                         HttpSession session,
                         Model model){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }
        model.addAttribute("previousURL", request.getHeader("referer"));
        return "reload";
    }


}
