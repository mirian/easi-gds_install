package com.example.interfacePredict.controller;

import com.example.interfacePredict.DTO.*;
import com.example.interfacePredict.services.Facade;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Controller used for the link prediction task.
 */
@org.springframework.stereotype.Controller
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ControllerLinkPrediction {
    @Autowired
    private Facade facade;

    /**
     * Function get of the url /linkPrediction
     * @param session of the type HttpSession.
     * @param model of the type Model.
     * @return String.
     */
    @GetMapping("/linkPrediction")
    public String linkPrediction(HttpSession session,
                                 Model model
    ){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }


        if(LocaleContextHolder.getLocale().toString().equals("en")){
            model.addAttribute("metaTitle", "Link Prediction");
        }
        else{
            model.addAttribute("metaTitle", "Prédiction de lien");
        }
        model.addAttribute("locale", LocaleContextHolder.getLocale().toString());
        model.addAttribute("css", "linkPrediction");
        model.addAttribute("active", "linkPrediction");
        model.addAttribute("pathDTO", new PathDTO());

        session.setAttribute("configML", new ConfigML());
        session.setAttribute("label", "");
        session.setAttribute("property", "");
        session.setAttribute("path", new ArrayList<String>());

        return "linkPrediction";
    }

    /**
     * Function get of the url /linkPredictionModel
     * @param session of the type HttpSession.
     * @param model of the type Model.
     * @return String.
     */
    @GetMapping("/linkPredictionModel")
    public String linkPredictionModel(HttpSession session,
                                      Model model
    ){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        if(LocaleContextHolder.getLocale().toString().equals("en")){
            model.addAttribute("metaTitle", "Link Prediction");
        }
        else{
            model.addAttribute("metaTitle", "Prédiction de lien");
        }
        model.addAttribute("locale", LocaleContextHolder.getLocale().toString());
        model.addAttribute("css", "linkPrediction");
        model.addAttribute("active", "linkPrediction");

        return "linkPrediction";
    }

    /**
     * Function get of the url /predictLinkPrediction
     * @param session of the type HttpSession.
     * @return String.
     */
    @GetMapping("/predictLinkPrediction")
    public String displayLinkPrediction(HttpSession session){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }
        return "redirect:/linkPrediction";
    }

    /**
     * Function post of the url /predictLinkPrediction
     * @param pathDTO of the type PathDTO.
     * @param features of the type List<String>.
     * @param context of the type List<String>.
     * @param typeResult of the type String.
     * @param numberResult of the type String.
     * @param writeModel of the type String.
     * @param session of the type HttpSession.
     * @param model of the type Model.
     * @return String.
     */
    @CrossOrigin
    @PostMapping("/predictLinkPrediction")
    public String displayLinkPrediction(@ModelAttribute("pathDTO") PathDTO pathDTO,
                                        @RequestParam(value = "features", required=false) List<String> features,
                                        @RequestParam(value = "context", required=false) List<String> context,
                                        @RequestParam("typeResult") String typeResult,
                                        @RequestParam(value = "numberResult", required=false) String numberResult,
                                        @RequestParam("writeModel") String writeModel,
                                        HttpSession session,
                                        Model model
    ){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        if(features == null){
            features = new ArrayList<>();
        }
        if(context == null){
            context = new ArrayList<>();
        }
        //At the beginning, we have one RF, MLP and LR with their default values.
        ConfigML configML1 = (ConfigML) session.getAttribute("configML");
        if(configML1.getRFEmpty() && configML1.getMLPEmpty() && configML1.getLREmpty()){
            configML1.addRF(new RandomForestDTO());
            configML1.addMLP(new MultiLinearPerceptronDTO());
            configML1.addLR(new LogisticRegression());
        }
        if(numberResult==""){
            numberResult = "5";
        }

        String query = "CALL easiGDS.templateLinkPredPath(" + pathDTO.toString() + "," +
                facade.listToString(features) +"," + "\"fastRP\"" + "," + configML1 + "," +
                "\"" + typeResult + "\"" + "," + "\"" + numberResult + "\"" + "," + facade
                .listToString(context) +  "," + writeModel + ") YIELD node1,node2,probability;";


        if(LocaleContextHolder.getLocale().toString().equals("en")){
            model.addAttribute("metaTitle", "Link Prediction");
        }
        else{
            model.addAttribute("metaTitle", "Prédiction de lien");
        }
        model.addAttribute("query", query);
        model.addAttribute("locale", LocaleContextHolder.getLocale().toString());
        model.addAttribute("css", "resultsLinkPrediction");
        model.addAttribute("active", "linkPrediction");

        return "resultsLinkPrediction";
    }

    /**
     * Function get of the url /popup
     * @param node1 of the type String.
     * @param node2 of the type String.
     * @param proba of the type String.
     * @param session of the type HttpSession.
     * @param model of the type Model.
     * @return String.
     */


    /**
     * Function get of the url /randomForestLinkPrediction
     * @param path of the type List<String>.
     * @param session of the type HttpSession.
     * @param model of the type Model.
     * @return String.
     */
    @GetMapping("/randomForestLinkPrediction")
    public String randomForest(@RequestParam("path") List<String> path,
                               HttpSession session,
                               Model model
    ){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        RandomForestDTO randomForestDTO = new RandomForestDTO();

        if(LocaleContextHolder.getLocale().toString().equals("en")){
            model.addAttribute("metaTitle", "Link Prediction");
        }
        else{
            model.addAttribute("metaTitle", "Prédiction de lien");
        }

        model.addAttribute("randomForestDTO", randomForestDTO);
        model.addAttribute("locale", LocaleContextHolder.getLocale().toString());
        model.addAttribute("css", "randomForest");
        model.addAttribute("active", "linkPrediction");

        session.setAttribute("path", path);

        return "randomForestLinkPrediction";
    }

    /**
     * Function post of the url /randomForestLinkPrediction
     * @param randomForestDTO of the type RandomForestDTO.
     * @param session of the type HttpSession.
     * @return String.
     */
    @PostMapping("/randomForestLinkPrediction")
    public String randomForest(@ModelAttribute("randomForestDTO") RandomForestDTO randomForestDTO,
                               HttpSession session
    ){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        ConfigML configML1 = (ConfigML) session.getAttribute("configML");
        if(randomForestDTO.notEmpty() || !configML1.getConfigEmpty() ){
            configML1.addRF(randomForestDTO);
        }

        session.setAttribute("configML", configML1);

        return "redirect:/linkPredictionModel";
    }

    /**
     * Function get of the url /multiLinearPerceptronLinkPrediction
     * @param path of the type List<String>.
     * @param session of the type HttpSession.
     * @param model of the type Model.
     * @return String.
     */
    @GetMapping("/multiLinearPerceptronLinkPrediction")
    public String multiLinearPerceptron(@RequestParam("path") List<String> path,
                                        HttpSession session,
                                        Model model
    ){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"), session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        MultiLinearPerceptronDTO multiLinearPerceptronDTO = new MultiLinearPerceptronDTO();

        model.addAttribute("multiLinearPerceptronDTO", multiLinearPerceptronDTO);
        model.addAttribute("locale", LocaleContextHolder.getLocale().toString());
        model.addAttribute("metaTitle", "multiLinearPerceptron");
        model.addAttribute("css", "multiLinearPerceptron");
        model.addAttribute("active", "linkPrediction");

        session.setAttribute("path", path);

        return "multiLinearPerceptronLinkPrediction";
    }

    /**
     * Function post of the url /multiLinearPerceptronLinkPrediction
     * @param multiLinearPerceptronDTO of the type MultiLinearPerceptronDTO.
     * @param session of the type HttpSession.
     * @return String.
     */
    @PostMapping("/multiLinearPerceptronLinkPrediction")
    public String multiLinearPerceptron(@ModelAttribute("multiLinearPerceptronDTO") MultiLinearPerceptronDTO multiLinearPerceptronDTO,
                                        HttpSession session
    ){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        ConfigML configML1 = (ConfigML) session.getAttribute("configML");
        if(multiLinearPerceptronDTO.notEmpty() || !configML1.getConfigEmpty()){
            configML1.addMLP(multiLinearPerceptronDTO);
        }

        session.setAttribute("configML", configML1);

        return "redirect:/linkPredictionModel";
    }

    /**
     * Function get of the url /logisticRegressionLinkPrediction
     * @param path of the type List<String>.
     * @param session of the type HttpSession.
     * @param model of the type Model.
     * @return String.
     */
    @GetMapping("/logisticRegressionLinkPrediction")
    public String logisticRegression(@RequestParam("path") List<String> path,
                                     HttpSession session,
                                     Model model
    ){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        LogisticRegression logisticRegressionDTO = new LogisticRegression();

        model.addAttribute("logisticRegressionDTO", logisticRegressionDTO);
        model.addAttribute("locale", LocaleContextHolder.getLocale().toString());
        model.addAttribute("metaTitle", "linkPrediction");
        model.addAttribute("css", "logisticRegression");
        model.addAttribute("active", "linkPrediction");

        session.setAttribute("path", path);

        return "logisticRegressionLinkPrediction";
    }

    /**
     * Function post of the url /logisticRegressionLinkPrediction
     * @param logisticRegressionDTO of the type LogisticRegression.
     * @param session of the type HttpSession.
     * @return String.
     */
    @PostMapping("/logisticRegressionLinkPrediction")
    public String logisticRegression(@ModelAttribute("logisticRegressionDTO") LogisticRegression logisticRegressionDTO,
                                     HttpSession session
    ){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        ConfigML configML1 = (ConfigML) session.getAttribute("configML");
        if(logisticRegressionDTO.notEmpty() || !configML1.getConfigEmpty()){
            configML1.addLR(logisticRegressionDTO);
        }

        session.setAttribute("configML", configML1);

        return "redirect:/linkPredictionModel";
    }

    /**
     * Reset the models enter by the user to default value
     * @param path
     * @param session
     * @return
     */
    @GetMapping("/linkPredictionModelsReset")
    public String linkPredictionModelsReset(@RequestParam("path") List<String> path,
                                        HttpSession session){

        session.setAttribute("configML", new ConfigML());
        session.setAttribute("path", path);

        return "redirect:/linkPredictionModel";
    }
}
