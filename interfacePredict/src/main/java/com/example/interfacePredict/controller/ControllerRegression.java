package com.example.interfacePredict.controller;

import com.example.interfacePredict.DTO.*;
import com.example.interfacePredict.services.Facade;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Controller used for the regression task.
 */
@org.springframework.stereotype.Controller
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ControllerRegression {
    @Autowired
    private Facade facade;

    /**
     * Function get of the url /regression
     * @param session of the type HttpSession.
     * @param model of the type Model.
     * @return String.
     */
    @GetMapping("/regression")
    public String regression(HttpSession session,
                             Model model
    ){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        if(LocaleContextHolder.getLocale().toString().equals("en")){
            model.addAttribute("metaTitle", "Regression");
        }
        else{
            model.addAttribute("metaTitle", "Régression");
        }
        model.addAttribute("css", "regression");
        model.addAttribute("active", "regression");
        model.addAttribute("locale", LocaleContextHolder.getLocale().toString());

        session.setAttribute("configML", new ConfigML());
        session.setAttribute("label", "");
        session.setAttribute("property", "");
        session.setAttribute("path", new ArrayList<String>());

        return "regression";
    }

    /**
     * Function get of the url /regressionModel
     * @param session of the type HttpSession.
     * @param model of the type Model.
     * @return String.
     */
    @GetMapping("/regressionModel")
    public String regressionModel(HttpSession session,
                                  Model model
    ){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        if(LocaleContextHolder.getLocale().toString().equals("en")){
            model.addAttribute("metaTitle", "Regression");
        }
        else{
            model.addAttribute("metaTitle", "Régression");
        }
        model.addAttribute("css", "regression");
        model.addAttribute("active", "regression");
        model.addAttribute("locale", LocaleContextHolder.getLocale().toString());

        return "regression";
    }

    /**
     * Function get of the url /predictRegression
     * @param session of the type HttpSession.
     * @return String.
     */
    @GetMapping("/predictRegression")
    public String displayRegression(HttpSession session){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }
        return "redirect:/regression";
    }

    /**
     * Function post of the url /predictRegression
     * @param label of the type String.
     * @param property of the type String.
     * @param write of the type String.
     * @param writeModel of the type String.
     * @param features of the type List<String>.
     * @param context of the type List<String>.
     * @param session of the type HttpSession.
     * @param model of the type Model.
     * @return String.
     */
    @CrossOrigin
    @PostMapping("/predictRegression")
    public String displayRegression(@RequestParam("label") String label,
                                    @RequestParam("property") String property,
                                    @RequestParam("write") String write,
                                    @RequestParam("writeModel") String writeModel,
                                    @RequestParam(value = "features", required=false) List<String> features,
                                    @RequestParam(value = "context", required=false) List<String> context,
                                    HttpSession session,
                                    Model model
    ){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        if(features == null){
            features = new ArrayList<>();
        }
        if(context == null){
            context = new ArrayList<>();
        }

        //At the beginning, we have one RF, MLP and LR with their default values.
        ConfigML configML1 = (ConfigML) session.getAttribute("configML");
        if(configML1.getRFEmpty() && configML1.getMLPEmpty() && configML1.getLREmpty()){
            configML1.addRF(new RandomForestDTO());
            configML1.addMLP(new MultiLinearPerceptronDTO());
            configML1.addLR(new LogisticRegression());
        }

        String query = "CALL easiGDS.templateNodeRegr(\"" + label + "\",\""  + property + "\"," +
                facade.listToString(features) +","  + configML1 + ","  + write + "," +
                facade.listToString(context) + "," + writeModel + ") YIELD node as node,prediction " +
                "as prediction return id(node),node,prediction;";


        if(LocaleContextHolder.getLocale().toString().equals("en")){
            model.addAttribute("metaTitle", "Regression");
        }
        else{
            model.addAttribute("metaTitle", "Régression");
        }
        model.addAttribute("query", query);
        model.addAttribute("css", "resultsRegression");
        model.addAttribute("active", "regression");
        model.addAttribute("locale", LocaleContextHolder.getLocale().toString());

        return "resultsRegression";
    }

    /**
     * Function get of the url /randomForestRegression
     * @param label of the type String.
     * @param property of the type String.
     * @param session of the type HttpSession.
     * @param model of the type Model.
     * @return String.
     */
    @GetMapping("/randomForestRegression")
    public String randomForestRegression(@RequestParam("label") String label,
                                         @RequestParam("property") String property,
                                         HttpSession session,
                                         Model model
    ){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        RandomForestDTO randomForestDTO = new RandomForestDTO();

        if(LocaleContextHolder.getLocale().toString().equals("en")){
            model.addAttribute("metaTitle", "Regression");
        }
        else{
            model.addAttribute("metaTitle", "Régression");
        }
        model.addAttribute("css", "randomForest");
        model.addAttribute("active", "regression");
        model.addAttribute("locale", LocaleContextHolder.getLocale().toString());
        model.addAttribute("randomForestDTO", randomForestDTO);

        session.setAttribute("label", label);
        session.setAttribute("property", property);

        return "randomForestRegression";
    }

    /**
     * Function post of the url /randomForestRegression
     * @param randomForestDTO of the type RandomForestDTO.
     * @param session of the type HttpSession.
     * @return String.
     */
    @PostMapping("/randomForestRegression")
    public String randomForestRegression(@ModelAttribute("randomForestDTO") RandomForestDTO randomForestDTO,
                                         HttpSession session
    ){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        ConfigML configML1 = (ConfigML) session.getAttribute("configML");
        if(randomForestDTO.notEmpty() || !configML1.getConfigEmpty()){
            configML1.addRF(randomForestDTO);
        }
        session.setAttribute("configML", configML1);

        return "redirect:/regressionModel";
    }

    /**
     * Function post of the url /linearRegression
     * @param linearRegressionDTO of the type LinearRegressionDTO.
     * @param session of the type HttpSession.
     * @return String.
     */
    @PostMapping("/linearRegression")
    public String linearRegression(@ModelAttribute("linearRegressionDTO") LinearRegressionDTO linearRegressionDTO,
                                   HttpSession session
    ){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        ConfigML configML1 = (ConfigML) session.getAttribute("configML");
        if(linearRegressionDTO.notEmpty() || !configML1.getConfigEmpty()){
            configML1.addLR(linearRegressionDTO);
        }
        session.setAttribute("configML", configML1);

        return "redirect:/regressionModel";
    }

    /**
     * Function get of the url /linearRegression
     * @param label of the type String.
     * @param property of the type String.
     * @param session of the type HttpSession.
     * @param model of the type Model.
     * @return String.
     */
    @GetMapping("/linearRegression")
    public String linearRegression(@RequestParam("label") String label,
                                   @RequestParam("property") String property,
                                   HttpSession session,
                                   Model model
    ){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"),
                session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        LinearRegressionDTO linearRegression = new LinearRegressionDTO();

        if(LocaleContextHolder.getLocale().toString().equals("en")){
            model.addAttribute("metaTitle", "Regression");
        }
        else{
            model.addAttribute("metaTitle", "Régression");
        }
        model.addAttribute("locale", LocaleContextHolder.getLocale().toString());
        model.addAttribute("css", "linearRegression");
        model.addAttribute("active", "regression");
        model.addAttribute("linearRegressionDTO", linearRegression);

        session.setAttribute("label", label);
        session.setAttribute("property", property);

        return "linearRegression";
    }

    /**
     * Reset the models enter by the user to default value
     * @param label
     * @param property
     * @param session
     * @return
     */
    @GetMapping("/regressionModelsReset")
    public String regressionModelsReset(@RequestParam("label") String label,
                                            @RequestParam("property") String property,
                                            HttpSession session){

        session.setAttribute("configML", new ConfigML());
        session.setAttribute("label", label);
        session.setAttribute("property", property);

        return "redirect:/regressionModel";
    }
}
