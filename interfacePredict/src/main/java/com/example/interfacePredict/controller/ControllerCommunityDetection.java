package com.example.interfacePredict.controller;

import com.example.interfacePredict.services.Facade;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller used for the community detection algorithms.
 */
@org.springframework.stereotype.Controller
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ControllerCommunityDetection {
    @Autowired
    private Facade facade;

    /**
     * Function get of the url /communityDetection
     * @param session of the type HttpSession.
     * @param model of the type Model.
     * @return String.
     */
    @GetMapping("/communityDetection")
    public String communityDetection(HttpSession session,
                                     Model model){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"), session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        if(LocaleContextHolder.getLocale().toString().equals("en")){
            model.addAttribute("metaTitle", "Community Detection");
        }
        else{
            model.addAttribute("metaTitle", "Détection de communauté");
        }
        model.addAttribute("locale", LocaleContextHolder.getLocale().toString());
        model.addAttribute("css", "communityDetection");
        model.addAttribute("active", "communityDetection");

        return "communityDetection";
    }

    /**
     * Function post of the url /communityDetection
     * @param algorithm of the type String.
     * @param session of the type HttpSession.
     * @param model of the type Model.
     * @return String.
     */
    @PostMapping("/communityDetection")
    public String resultCommunityDetection(@RequestParam("algorithm") String algorithm,
                                           HttpSession session,
                                           Model model){
        if(!facade.testConnected(session.getAttribute("userName"), session.getAttribute("pwd"), session.getAttribute("port"),session.getAttribute("address"),session.getAttribute("driver"))){
            return "redirect:/";
        }

        model.addAttribute("metaTitle", "Cypher");
        model.addAttribute("locale", LocaleContextHolder.getLocale().toString());
        model.addAttribute("css", "cypher");
        model.addAttribute("active", "cypher");

        String query = "CALL easiGDS." + algorithm + "() YIELD nodeId, communityId RETURN gds.util.asNode(nodeId) AS" +
                " node, communityId ORDER BY communityId ASC;";
        model.addAttribute("query", query);

        return "cypher";
    }
}
