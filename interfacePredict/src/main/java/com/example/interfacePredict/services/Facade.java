package com.example.interfacePredict.services;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * This class contains utils function.
 */
@Service
public class Facade {
    private Integer lastIdUser = 0;

    /**
     * Transform the java list in neo4j list structure.
     * @param list type of List<String>.
     * @return String.
     */
    public  String listToString(List<String> list){
        String res = "[";
        for(String elem : list){
            res += "\"" + elem + "\",";
        }
        if(res.charAt(res.length() - 1) == (',')){
            res = res.substring(0, res.length() - 1);
        }
        res += "]";
        return res;
    }

    /**
     * Test if the user is connected to the database.
     * @param userName type of Object, the name of the user in the neo4j database.
     * @param pwd type of Object, the password of the user in the neo4j database.
     * @param port type of Object, the port of the neo4j database.
     * @return Boolean.
     */
    public Boolean testConnected(Object userName, Object pwd, Object port,Object address,Object driver){
        return userName != null && pwd != null && port != null&&address!=null&&driver!=null;
    }

    /**
     * Return an new id for a new user.
     * @return Integer.
     */
    public Integer getLastIdUser(){
        this.lastIdUser ++;
        return this.lastIdUser;
    }
}
