package com.example.interfacePredict;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.thymeleaf.spring6.view.ThymeleafViewResolver;

@SpringBootApplication
public class InterfacePredictApplication {

    public static void main(String[] args) {
        SpringApplication.run(InterfacePredictApplication.class, args);
    }

}
