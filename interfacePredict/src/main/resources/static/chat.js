$(document).ready(function(){
    let messageList = document.getElementById("divMessageList");

    const urlState ="http://localhost:8080/chatBotState"
    const optionsState = {
        method: 'GET',

    };
    fetch(urlState,optionsState).then(state =>{
        if(state.headers.get("state")=="true"){
            $(".Layout").toggle();
            $(".chat_on").hide(300);
            loadHistorique();
            messageList.scrollTop = messageList.scrollHeight;
        }
        $(".chat_on").click(function(){
            const url ="http://localhost:8080/chatBotState?chatBotState=true"
            const options = {
                method: 'POST',

            };
            fetch(url,options).then(status =>{
                $(".Layout").toggle();
                $(".chat_on").hide(300);
                loadHistorique();
                messageList.scrollTop = messageList.scrollHeight;
            })

        });

        $(".chat_close_icon").click(function(){
            const url ="http://localhost:8080/chatBotState?chatBotState=false"
            const options = {
                method: 'POST',

            };
            fetch(url,options).then(status => {
                $(".Layout").hide();
                $(".chat_on").show(300);
            })
        });
        let input = document.getElementById("inputChat");
        input.addEventListener("keypress", function(event) {
            if (event.key === "Enter") {
                event.preventDefault();
                sendMessage();
            }
        });
    })


});

/**
 * Use to send the message to Flask server and show the question and response in the chatbox
 */
function sendMessage(){
    let message = document.getElementById("inputChat").value;

    if(message!=""){
        document.getElementById("inputChat").value = "";
        questionFlask(message).then(response => {
            addQuestion(message);
            addResponse(response.response, response.link);
        })

    }
}

/**
 * Add the question in the chat box
 * @param {String} questionMessage the question to show
 */
function addQuestion(questionMessage){
    let messageList = document.getElementById("divMessageList");
    let question = document.createElement("p");
    let div = document.createElement("div");

    question.setAttribute("class","question");
    question.innerHTML = questionMessage;

    div.setAttribute("class","divQuestion");
    div.appendChild(question);

    messageList.appendChild(div);
    messageList.scrollTop = messageList.scrollHeight;

}

/**
 * Use to show a response in the chat box
 * @param {String} responseMessage the responseMessage to show
 * @param {String} link a link associate with the response (can be empty)
 */
function addResponse(responseMessage, link){
    console.log(link);
    let messageList = document.getElementById("divMessageList");
    let div = document.createElement("div");
    let response = document.createElement("p");

    response.setAttribute("class","reponse");
    response.innerHTML =responseMessage;

    div.setAttribute("class", "divResponse");
    div.appendChild(response);

    messageList.appendChild(div);
    if(link != ""){
        let form = document.createElement("form");
        let button = document.createElement("button");
        let i = document.createElement("i");
        let divLink = document.createElement("div");
        let pLink = document.createElement("p");

        divLink.setAttribute("class", "divResponse");

        form.setAttribute("method", "post");
        form.setAttribute("action", link);

        button.setAttribute("type", "submit");
        button.setAttribute("class", "buttonLaunchPredict");

        i.setAttribute("class", "fa-solid fa-rocket");
        i.setAttribute("style","color : #474747;")

        pLink.setAttribute("class", "reponse");

        button.appendChild(i);
        pLink.appendChild(button);
        form.appendChild(pLink);
        divLink.appendChild(form);
        messageList.appendChild(divLink);
    }
    messageList.scrollTop = messageList.scrollHeight;
}

/**
 * Use to send question to Flask server and return the response
 * @param {String} question the question
 * @returns {Promise<any|string>} the response of Flask server
 */
async function questionFlask(question){
    let idUser = document.cookie
        .split("; ")
        .find((row) => row.startsWith("idUser="))
        ?.split("=")[1];
    const url = 'http://localhost:8000/messages?idUser=' + idUser + "&database=" + db + '&question=' + question;
    const options = {
        method: 'GET',
        headers: {
            "dataType" : "json",
            'Content-Type': 'application/json'
        },
    };
    try {
        const response = await fetch(url,options);
        if (!response.ok) {
            throw new Error(`Error from the HTTP response : ${response.status}`);
        }
        const data = await response.json();
        return data;
    } catch (error) {
        return {response: "Error: cannot connect to the chatbot server!", link: ""};
    }
}

/**
 * Use to load the historique message
 * @returns {Promise<void>}
 */
async function loadHistorique(){
    let idUser = document.cookie
        .split("; ")
        .find((row) => row.startsWith("idUser="))
        ?.split("=")[1];
    const url = 'http://localhost:8000/history?idUser=' + idUser + "&database=" + db;
    const options = {
        method: 'GET',
        headers: {
            "dataType" : "json",
            'Content-Type': 'application/json'
        },
    };
    try {
        const response = await fetch(url,options);
        if (!response.ok) {
            throw new Error(`Error from the HTTP response : ${response.status}`);
        }
        const data = await response.json();
        data["response"].forEach(message=>{
            if(message["type"] == 0){
                addQuestion(message["content"]);
            }
            else{
                addResponse(message["content"], message["link"]);
            }
        })
    } catch (error) {
        console.log(`Error from the request : ${error}`);
    }
}