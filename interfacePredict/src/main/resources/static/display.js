let graph;

/**
 * Fonction use to init the graph on the right of some page
 * @param {String} cypher the cypher request send to neo4j database
 * @returns {Promise<void>}
 */
async function init(cypher){


    const url ="http://localhost:8080/driverNeo4jConnect"
    const options = {
        method: 'POST',
        headers: {
            'Accept': 'application/json;charset=UTF-8',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({"cypher":"CALL db.schema.nodeTypeProperties() YIELD nodeLabels, propertyName, propertyTypes"})

    };
    let error = false
    fetch(url,options)
        .then(
            response =>{
                if(response.status == 400){
                    error=true;
                }
                return response.json()
            })
        .then(result => {
            console.log(result)
            result.records.forEach(record => {

                if(dico[record["nodeLabels"]]==null){
                    dico[record["nodeLabels"]] = []
                }
                dico[record["nodeLabels"]].push([record["propertyName"],record["propertyTypes"]])
            })
        })
        .catch(error => {
            console.log(error)
        })

    let neo4jd3 = Neo4jD3('#neo4jd3', {

        nodeRadius: 25,
        minCollision: 60,
        neo4jData: {results:[]},
        infoPanel:false,

        onNodeClick: function(node) {
            showProperties(node["properties"]["name"])
        },
        onRelationshipDoubleClick: function(relationship) {
            console.log('double click on relationship: ' + JSON.stringify(relationship));
        },
        zoomFit: true,
    });
    graph = neo4jd3;

    loadNeo4JData(neo4jd3,cypher);
}

/**
 * Show an alert with information on a node label of databse schema
 * @param {String} label the label of the node we going to display
 */
function showProperties(label){
    let res = "";
    if(dico[label.toString()][0][0]==null){
        res = "No properties";
    }
    else{
        dico[label.toString()].forEach(elem => {
            res += elem[0] +":"+ elem.slice(1,elem.length).toString()+"\n";
        })
    }
    let t = "Properties of "+label;

    swal({
        title:t,
        text:res,
        button: true,
    })
}

/**
 * Load the neo4J data on the webpage
 * @param {Neo4jD3} neo4jd3 the div where we load data
 * @param {String} cypher the cypher query send to database
 * @returns {Promise<void>}
 */
async function loadNeo4JData(neo4jd3,cypher){

    const res = await fetch("httpNeo4jRequest", {
        method: 'POST',
        headers: {
            'Accept': 'application/json;charset=UTF-8',
            'Content-Type': 'application/json'

        },
        body: JSON.stringify({"cypher":JSON.stringify(cypher)})
    })

    res.json().then(data=> {
        console.log(data);
        neo4jd3.updateWithNeo4jData(data)
    });
}

/**
 * Use to load the data of a cypher request in a table div
 * @param {Object} data the reponse data of neo4j request
 */
function createTableRawData(data){
    let div = document.getElementById("rawData");
    div.innerHTML = "";
    let table = document.createElement("table");
    table.setAttribute("class","table");

    //Head of table
    let thead = document.createElement("thead");
    let trhead = document.createElement("tr");
    let th1 = document.createElement("th");
    th1.setAttribute("scope","col");
    th1.innerText="#";
    trhead.appendChild(th1);
    let resultData = data.results[0];
    resultData.columns.forEach(elem =>{
        let th2 = document.createElement("th");
        th2.setAttribute("scope","col");
        th2.innerText=elem;
        trhead.appendChild(th2);
    })
    thead.appendChild(trhead);
    table.appendChild(thead);

    //Body of table
    let tbody = document.createElement("tbody");
    tbody.setAttribute("id","table");
    let index = 1;
    resultData.data.forEach(elem => {
        let trbody = document.createElement("tr");
        trbody.setAttribute("name","tr"+index);
        let thIndex = document.createElement("th");
        thIndex.setAttribute("scope","row");
        thIndex.innerText=""+index;
        trbody.appendChild(thIndex);

        for(let i = 0;i<elem.row.length;i++){
            let tdBody = document.createElement("td");
            if(elem.meta[i]==null) {
                if (typeof row == "object") {
                    tdBody.innerText = JSON.stringify(elem.row[i]);
                } else {
                    tdBody.innerText = elem.row[i].toString();
                }
                trbody.appendChild(tdBody);
            }
            else{
                if(elem.meta[i].type == "node"){
                    elem.graph.nodes.forEach(node=>{
                        if(node.id == elem.meta[i].id){
                            tdBody.innerText = JSON.stringify(node.labels) + " : " + JSON.stringify(node.properties)
                        }
                    })
                }
                if(elem.meta[i].type == "relationship"){
                    elem.graph.relationships.forEach(rel=>{
                        if(rel.id == elem.meta[i].id){
                            tdBody.innerText = rel.type + " : " + JSON.stringify(rel.properties)
                        }
                    })
                }
                trbody.appendChild(tdBody);
            }

        };
        tbody.appendChild(trbody);
        index += 1;

    })
    table.appendChild(tbody);

    div.appendChild(table);
}

/**
 * Use to execute the nodeClassification query and display the result
 * @param {String} cypher the cypher query send to neo4J database
 * @returns {Promise<void>}
 */
async function execClass(cypher){
    let div = document.getElementById("neo4jd3");
    div.innerHTML='<img class="load imgLoad" src="./img/Load.gif" />';

    const res = await fetch("httpNeo4jRequest", {
        method: 'POST',
        headers: {
            'Accept': 'application/json;charset=UTF-8',
            'Content-Type': 'application/json'

        },
        body: JSON.stringify({"cypher":JSON.stringify(cypher)})
    })
    res.json().then(function(dataR){
        console.log(dataR);
        if(dataR.errors.length!=0){
            errorAlert(dataR.errors[0]);
            div.innerHTML="";
            return;
        }
        if(dataR.notifications){
            warningAlert(dataR.notifications[0])
        }
        if(dataR.results.length!=0) {

            createTableRawData(dataR);

            let neo4jd3 = Neo4jD3('#neo4jd3', {

                nodeRadius: 25,
                minCollision: 60,
                neo4jData: dataR,

                onNodeClick: function (node) {
                    let t = "Predicted class for Node<" + node["id"] + "> : " + dicoRes[node["id"]];
                    swal({
                        title: t,
                        icon: "info",
                        button: false,
                        timer: 5000
                    })
                },
                onRelationshipDoubleClick: function (relationship) {
                    console.log('double click on relationship: ' + JSON.stringify(relationship));
                },
                zoomFit: true,
                infoPanel: true
            })
            graph = neo4jd3;

            dataR.results[0].data.forEach(element => {
                dicoRes[element.graph.nodes[0]["id"]] = element.row[dataR.results[0].columns.indexOf("prediction")];
            });

        }
    })
}

/**
 * Use to execute the nodeClassification query and display the result
 * @param {String} cypher The cypher query send to neo4J database
 * @returns {Promise<void>}
 */
async function execRegr(cypher){
    let div = document.getElementById("neo4jd3");
    div.innerHTML='<img class="load imgLoad" src="./img/Load.gif" />';



    const res = await fetch("httpNeo4jRequest", {
        method: 'POST',
        headers: {
            'Accept': 'application/json;charset=UTF-8',
            'Content-Type': 'application/json'

        },
        body: JSON.stringify({"cypher":JSON.stringify(cypher)})
    })

    res.json().then(function(dataR){
        if(dataR.errors.length!=0){
            errorAlert(dataR.errors[0]);
            div.innerHTML="";
            return;
        }
        if(dataR.notifications){
            warningAlert(dataR.notifications[0])
        }
        if(dataR.results.length!=0) {
            createTableRawData(dataR);

            let neo4jd3 = Neo4jD3('#neo4jd3', {

                    nodeRadius: 25,
                    minCollision: 60,
                    neo4jData: dataR,

                    onNodeClick: function (node) {
                        t = "Predicted value for Node<" + node["id"] + "> : " + dicoRes[node["id"]];
                        swal({
                            title: t,
                            icon: "info",
                            button: false,
                            timer: 5000
                        })
                    },
                    onRelationshipDoubleClick: function (relationship) {
                        console.log('double click on relationship: ' + JSON.stringify(relationship));
                    },
                    zoomFit: true,
                    infoPanel: true
                });

            graph = neo4jd3;
            console.log(dataR);
            dataR.results[0].data.forEach(element => {
                dicoRes[element.graph.nodes[0]["id"]] = element.row[dataR.results[0].columns.indexOf("prediction")];
            });
        }
    })
}

/**
 * Use to execute the nodeClassification query and display the result
 * @param {String}cypher the cypher query send to neo4J database
 * @returns {Promise<void>}
 */
async function execLinkPred(cypher){
    let div = document.getElementById("table");
    div.innerHTML='<img class="load imgLoad" src="./img/Load.gif" />';

    const res = await fetch("httpNeo4jRequest", {
        method: 'POST',
        headers: {
            'Accept': 'application/json;charset=UTF-8',
            'Content-Type': 'application/json'

        },
        body: JSON.stringify({"cypher":JSON.stringify(cypher)})
    })


        res.json().then(function(dataR){
            console.log(dataR);
            if(dataR.errors.length!=0){
                errorAlert(dataR.errors[0]);
                div.innerHTML="";
                return;
            }
            if(dataR.notifications){
                warningAlert(dataR.notifications[0])
            }
            if(dataR.results.length!=0){
                if(dataR.results[0].data.length == 0){

                    let empty = document.createElement("p");
                    let i = document.createElement("i");
                    let subDiv1 = document.createElement("div");
                    let subDiv2 = document.createElement("div");
                    let subDiv3 = document.createElement("div");

                    subDiv1.setAttribute("class", "row");
                    subDiv2.setAttribute("class", "col");
                    subDiv3.setAttribute("class", "col");
                    subDiv3.setAttribute("id", "p");

                    empty.innerText = emptyText;

                    i.setAttribute("class", "fa-sharp fa-solid fa-circle-exclamation");
                    i.setAttribute("style", "color: #ff0f0f;");

                    div.innerHTML = "";
                    subDiv2.appendChild(i);
                    subDiv3.appendChild(empty);
                    subDiv1.appendChild(subDiv2);
                    subDiv1.appendChild(subDiv3);
                    div.appendChild(subDiv1);
                }
                else {
                    document.getElementById("searchIdDiv").hidden = false;
                    console.log(dataR);

                    let tableLink = document.createElement("table");
                    tableLink.setAttribute("class", "table");

                    //Head of table
                    let thead = document.createElement("thead");
                    let trhead = document.createElement("tr");
                    let th1 = document.createElement("th");
                    th1.setAttribute("scope", "col");
                    th1.innerText = "#";
                    trhead.appendChild(th1);

                    let thN1 = document.createElement("th");
                    thN1.setAttribute("scope", "col");
                    thN1.innerText = "Node 1";
                    trhead.appendChild(thN1);

                    let thN2 = document.createElement("th");
                    thN2.setAttribute("scope", "col");
                    thN2.innerText = "Node 2";
                    trhead.appendChild(thN2);

                    let thProb = document.createElement("th");
                    thProb.setAttribute("scope", "col");
                    thProb.innerText = "Probability";
                    trhead.appendChild(thProb);

                    let thButton = document.createElement("th");
                    thButton.setAttribute("scope", "col");
                    thButton.innerText = "See Results";
                    trhead.appendChild(thButton);

                    thead.appendChild(trhead);
                    tableLink.appendChild(thead);

                    //Body of table
                    let tbody = document.createElement("tbody");
                    tbody.setAttribute("id", "table");
                    let indexLink = 1;
                    dataR.results[0].data.forEach(elem => {

                        let id1 = elem.graph.nodes[0].id;
                        let id2 = elem.graph.nodes[1].id;
                        let tid1 = elem.graph.nodes[0].labels.toString() + " : " + elem.graph.nodes[0].id;
                        let tid2 = elem.graph.nodes[1].labels.toString() + " : " + elem.graph.nodes[1].id;
                        let p = elem.row[2];
                        let cypher = "return gds.util.asNodes([" + id1 + "," + id2 + "])";

                        let trbody = document.createElement("tr");
                        trbody.setAttribute("name", id1 + "/" + id2);
                        let thIndex = document.createElement("th");
                        thIndex.setAttribute("scope", "row");
                        thIndex.innerText = "" + indexLink;
                        trbody.appendChild(thIndex);

                        let tdN1 = document.createElement("td");
                        tdN1.innerText = tid1;
                        trbody.appendChild(tdN1);

                        let tdN2 = document.createElement("td");
                        tdN2.innerText = tid2;
                        trbody.appendChild(tdN2);

                        let tdP = document.createElement("td");
                        tdP.innerText = p.toPrecision(3);
                        trbody.appendChild(tdP);

                        let tdButton = document.createElement("td");
                        let centerButton = document.createElement("center");
                        let button = document.createElement("button");
                        button.setAttribute("class", "btn");
                        button.setAttribute("onclick", "loadResLinkPred(" + id1 + "," + id2 + "," + p + ")");
                        let icon = document.createElement("i");
                        icon.setAttribute("class", "fa-solid fa-circle-play fa-lg");
                        icon.setAttribute("style", "color: #1ed74d;");
                        button.appendChild(icon);
                        centerButton.appendChild(button);
                        tdButton.appendChild(centerButton);

                        trbody.appendChild(tdButton)

                        tbody.appendChild(trbody);
                        indexLink += 1;

                    })
                    tableLink.appendChild(tbody);

                    div.innerHTML = "";
                    div.appendChild(tableLink);
            }
            }}
    )}

/**
 * Use to load the popup on the result of link prediction
 * @param {int} node1 the id of the first node
 * @param {int} node2 the id of the second node
 * @param {float} p the probability of the link
 */

async function loadResLinkPred(node1, node2, proba){
    let div = document.getElementById("neo4jd3")
    div.innerHTML='<img class="imgLoad" src="load.gif" />'

    cypher="return gds.util.asNodes(["+node1+","+node2+"])";
    const res = await fetch("httpNeo4jRequest", {
        method: 'POST',
        headers: {
            'Accept': 'application/json;charset=UTF-8',
            'Content-Type': 'application/json'

        },
        body: JSON.stringify({"cypher":JSON.stringify(cypher)})
    })
    res.json().then(function(dataR){

        console.log(dataR)
        let id1 = dataR.results[0].data[0].graph.nodes[0].id
        let id2 = dataR.results[0].data[0].graph.nodes[1].id
        dataR.results[0].data[0].graph.relationships=[{"id":"1","type":"PREDICTION","startNode":id1.toString(),"endNode":id2.toString(),"properties":{"probability":proba.toString()}}]

        let neo4jd3 = Neo4jD3('#neo4jd3', {

            nodeRadius: 25,
            minCollision: 60,
            neo4jData: dataR,

            onRelationshipDoubleClick: function(relationship) {
                let t = "Probability of link between Node<"+id1+"> and Node<"+id1+">  : "+proba;
                swal({
                    title:t,
                    icon:"info",
                    button:false,
                    timer:5000
                })
            },
            zoomFit: true,
            infoPanel:true


        });
        graph = neo4jd3;
    });
}
/**
 * Use to find specific node in the linkPrediction result page
 */
function searchLinkPred(){
    let id = document.getElementById("InputId").value;
    let div = document.getElementById("table");
    let tableRow = div.childNodes[0].childNodes[1].children;
    let arrayRow = Array.from(tableRow);

    arrayRow.filter(row =>{
        let arrayId = row.attributes[0].nodeValue.split("/");
        if(id==""){
            row.hidden=false;
        }
        else {
            if (arrayId[0] == (id) || arrayId[1] == (id)) {
                row.hidden = false
            } else {
                row.hidden = true;
            }
        }
    })
}

/**
 * Use to execute a cypher request on neo4J database
 * @param cypher {String} the cypher request
 * @param scale {String} use to say if we scale the node size on a property (for the centrality results)
 * @returns {Promise<void>}
 */
async function execCypher(cypher,scale="false"){
    let div = document.getElementById("neo4jd3");
    div.innerHTML='<img class="load imgLoad" src="./img/Load.gif" />';



    const res = await fetch("httpNeo4jRequest", {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({"cypher":JSON.stringify(cypher)})
    })

    res.json().then(function(dataR){
        console.log(dataR);
        if(dataR.errors.length!=0){
            errorAlert(dataR.errors[0]);
            div.innerHTML="";
            return;
        }
        if(dataR.notifications){
            warningAlert(dataR.notifications[0])
        }
        if(dataR.results.length!=0) {
            let containsGraph = dataContainsGraph(dataR);
            createTableRawData(dataR);
            let neo4jd3;
            if (scale == "true") {
                dataR = scaleData(dataR);
                neo4jd3 = Neo4jD3('#neo4jd3', {

                    nodeRadius: 25,
                    minCollision: 60,
                    neo4jData: {results: []},

                    onRelationshipDoubleClick: function (relationship) {
                        console.log('double click on relationship: ' + JSON.stringify(relationship));
                    },
                    zoomFit: true,
                    infoPanel: true,
                    propScale: "scale"

                })
                neo4jd3.updateWithNeo4jData(dataR, "scale")
            } else {
                neo4jd3 = Neo4jD3('#neo4jd3', {

                    nodeRadius: 25,
                    minCollision: 60,
                    neo4jData: dataR,

                    onRelationshipDoubleClick: function (relationship) {
                        console.log('double click on relationship: ' + JSON.stringify(relationship));
                    },
                    zoomFit: true,
                    infoPanel: true
                })
            }
            graph = neo4jd3;
            if (!containsGraph) {
                showTableMode();
            } else {
                if (dataR.results[0].data.length > 500) {
                    showTableMode();
                } else {
                    showGraphMode();
                }
            }


        }
    })
}

/**
 * Use to in centrality to put a score in range r1 in range r2  to adjust the size of node by its score results
 * @param {float} value the value in the range r1
 * @param {float} r1 the range of our raw value
 * @param {float} r2 the range where we want our value
 * @returns {float} the value of 'value' in range r2
 */
function convertRange( value, r1, r2 ) {
    return ( value - r1[ 0 ] ) * ( r2[ 1 ] - r2[ 0 ] ) / ( r1[ 1 ] - r1[ 0 ] ) + r2[ 0 ];
}

/**
 * Use to put score and scale information in database without committing them in neo4j database
 * @param {Object} data data results send by neo4J database
 * @returns {Object} the data with the scale and score information
 */
function scaleData(data){
    let listVal = [];
    data.results[0].data.forEach(n=>{

        listVal.push(n.row[1])
    })

    let minL = Math.min(...listVal);
    let maxL = Math.max(...listVal);

    data.results[0].data.forEach(n=>{
        n.graph.nodes[0].properties["score"]=n.row[1]
        if(maxL-minL == 0){
            n.graph.nodes[0].properties["scale"]=1;
        }
        else{
            n.graph.nodes[0].properties["scale"]= convertRange( n.row[1], [ minL, maxL ], [ 0.8, 2 ] );
        }
    })
    return data;
}

/**
 * Use to switch between the graph mode and the table mode in results page
 */
function switchDisplayMode(){
    let graphDiv = document.getElementById("neo4jd3");
    let tableDiv = document.getElementById("rawData");

    if(tableDiv.hidden){
        graphDiv.hidden=true;
        tableDiv.hidden=false;
    }
    else{
        tableDiv.hidden=true;
        graphDiv.hidden=false;
    }
}

/**
 * Use to show the graphMode in results page
 */
function showGraphMode(){
    let graphDiv = document.getElementById("neo4jd3");
    let tableDiv = document.getElementById("rawData");
    tableDiv.hidden=true;
    graphDiv.hidden=false;
    document.getElementById("switchResMode").checked = true;
}

/**
 * Use to show the tableMode in results page
 */
function showTableMode(){
    let graphDiv = document.getElementById("neo4jd3");
    let tableDiv = document.getElementById("rawData");
    tableDiv.hidden=false;
    graphDiv.hidden=true;
    document.getElementById("switchResMode").checked = false;
}

/**
 * Use to check if a data result send by neo4j contains a graph or not
 * @param {Object} data the data result send by neo4j
 * @returns {boolean} True if contains a graph false otherwise
 */
function dataContainsGraph(data){
    let res = false;
    data.results[0].data.every(elem => {
        if(!(elem.meta[0]==null)){
            res = true;
            return;
        }
    })
    return res;
}

/**
 * Use to show an alert on a neo4J data results
 * @param {Object} error the error in neo4J data results
 */
function errorAlert(error){

    swal({
        title:error.code,
        text:error.message,
        icon:"error",
        button: true,
    })
}
/**
 * Use to show a warning on a neo4J data results
 * @param {Object}warning the warning in neo4J data results
 */
function warningAlert(warning){
    swal({
        title:warning.title,
        text:warning.description,
        icon:"warning",
        button: true,
    })
}