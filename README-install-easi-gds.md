## **Steps to install EASI-GDS:** 

1. Click on Download, download the easi-gds_install-main.zip and unzip it

2. If you have downloaded Docker on your computer, start it and go to 3; if not, go to 9 (optional step) to install Docker Desktop and start it.

3. Open the terminal in the same directory as docker-compose.yml

4. Execute: `docker compose up`

5. Open  **localhost:7474** in your browser and login in with  **name: neo4j, password: password**

6. Open the file ‘_cypher.txt_’ and copy-paste all the queries to import data
* If you face the problem here, please go to the file ‘docker-compose.yml’ and make sure ‘./neo4j/import’ for volumes is the same directory as the folder ‘neo4j/import’ in easi-gds_install-main. 

* You can use '`sudo chown -R new-owner file/folderName`' to change the owner of a folder or a file if you need it.

7. Open **localhost:8080** or  **localhost:8000** in your browser

8. Login with  **name:neo4j, password:password, Database Address:host.docker.internal, Database port:7474**

* There is a country icon at the top-right corner of the interface to change language.

* If you have the problem about _$JAVA_HOME_, please go to the file ‘_dockerfile_app_’ and make sure it’s the same as the JAVA jdk path in _/usr/lib/jvm/_. 

Now, you enter into the world EASI-GDS.


## (Optional) 9: Install Docker Desktop
If you don’t have Docker on your computer, here is the address https://docs.docker.com/desktop/install/ubuntu/  to download it on Ubuntu, where  you can also find the method for MAC, Windows etc.

1) Make sure your computer meet the system requirements
2) Set up Docker's package repository
3) Download latest DEB package
4) Install the package with apt as follows:

    `sudo apt-get update`

    `sudo apt-get install ./docker-desktop-<version>-<arch>.deb`

* you can find version and arch in the name or property of DEB package

